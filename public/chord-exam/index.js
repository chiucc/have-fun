const assignNewTask = require('./app/AssignTask').assignNewTask;

const CheckAnswerBtnState = {
  Go: 'Correct？',
  Next: 'Next',
  Retry: 'Retry'
};

const ShowAnswerState = {
  Show: 'Answer',
  Hide: 'Hide'
};

const Reg = {
  Input: /[A-G][b|\#]?/, // allow 'B', 'C#', 'Fb'
  Key: /[A-G]/,
  Slash: /\//
};

const checkboxState = {
  triadMajor: true,
  triadMinor: true,
  triadDiminished: true,
  seventhMajor: true,
  seventhMinor: true,
  seventhDominant: true,
  seventhHalfDiminished: true,
  seventhDiminished: true
};

let currAnswer = [];

// DOM handler
(function() {
  /**
   *
   *  @param {String} inputString
   *  @return {Array}
   *  @example
   *  parseUserInput('A/C#/E/G')  // ['A', 'C#', 'E', 'G']
   */
  function parseUserInput(inputString) {
    // console.log(inputString);
    return inputString
      .trim()
      .split('/')
      .filter(char => Reg.Input.test(char));
  }

  function slashAutocomplete() {
    let userInput = document.querySelector('#userAttempt-input'),
      value = userInput.value.split('');
    if (value.length === 1) {
      return;
    }

    // try value.match(/[A-G]/g)
    let numOfKey = value.filter(char => Reg.Key.test(char)).length,
      numOfSlash = value.filter(char => Reg.Slash.test(char)).length,
      isLackOneSlashPattern = numOfKey - numOfSlash === 2, // eg: AC => A/C, G/BD=> G/B/D
      lastIndex = value.length - 1,
      lastChar = value[lastIndex],
      isLastCharKey = Reg.Key.test(lastChar);

    if (isLackOneSlashPattern && isLastCharKey) {
      userInput.value = userInput.value.substr(0, lastIndex) + '/' + lastChar;
    }
  }

  function showAnswer(toShow, btnInnerHTML) {
    let answerSection = document.querySelector('#question-answer');
    answerSection.style.display = toShow ? 'flex' : 'none';

    let getAnswerBtn = document.querySelector('#get-answer-btn');
    getAnswerBtn.innerHTML = btnInnerHTML;
  }

  function showSettingWindow(toShow) {
    let modal = document.querySelector('#myModal');
    modal.style.display = toShow ? 'block' : 'none';
  }

  function assignNewTaskAndResetStyle() {
    // hide answer
    showAnswer(false, ShowAnswerState.Show);
    // reset input value
    let userInput = document.querySelector('#userAttempt-input');
    userInput.value = '';
    userInput.classList.remove('wrong-answer');
    userInput.classList.remove('correct-answer');
    // reset btn
    let checkAnswerBtn = document.querySelector('#check-answer-btn');
    checkAnswerBtn.innerHTML = CheckAnswerBtnState.Go;
    // assign new task
    currAnswer = assignNewTask(checkboxState);
  }

  function addListener() {
    let checkAnswerBtn = document.querySelector('#check-answer-btn');
    checkAnswerBtn.addEventListener('click', () => {
      let userInput = document.querySelector('#userAttempt-input');
      switch (checkAnswerBtn.innerHTML) {
        case CheckAnswerBtnState.Go:
        case CheckAnswerBtnState.Retry:
          // check answer
          let inputInArray = parseUserInput(userInput.value),
            isCorrect =
              inputInArray.length === currAnswer.length &&
              currAnswer.every((char, i) => char === inputInArray[i]);

          if (isCorrect) {
            userInput.classList.remove('wrong-answer');
            userInput.classList.add('correct-answer');
            checkAnswerBtn.innerHTML = CheckAnswerBtnState.Next;
          } else {
            userInput.classList.remove('correct-answer');
            userInput.classList.add('wrong-answer');
            checkAnswerBtn.innerHTML = CheckAnswerBtnState.Retry;
          }
          break;
        case CheckAnswerBtnState.Next:
          assignNewTaskAndResetStyle();
          break;
        default:
          break;
      }
    });

    let getAnswerBtn = document.querySelector('#get-answer-btn');
    getAnswerBtn.addEventListener('click', () => {
      let toShow = getAnswerBtn.innerHTML === ShowAnswerState.Show,
        btnInnerHTML = toShow ? ShowAnswerState.Hide : ShowAnswerState.Show;
      showAnswer(toShow, btnInnerHTML);
    });

    let applySettingBtn = document.querySelector('#apply-setting-btn');
    applySettingBtn.addEventListener('click', () => {
      showSettingWindow(false);
      // whenever setting change, assign new task
      assignNewTaskAndResetStyle();
    });

    let settingBtn = document.querySelector('#setting-btn');
    settingBtn.addEventListener('click', () => showSettingWindow(true));

    // checkboxes setting

    // event delegation? but how?
    // let requirement = document.querySelector('#requirement');
    // requirement.addEventListener('click', () => {
    // });

    function addListenerOnCheckbox(domID, checkboxProperty) {
      let domElement = document.querySelector(domID);
      domElement.addEventListener('click', () => {
        // 要再檢查 checkboxState[checkboxProperty] 是不是目前唯一有選的, 要保證至少有一個 checkbox 打勾

        checkboxState[checkboxProperty] = domElement.checked;
      });
    }

    addListenerOnCheckbox('#triad-major', 'triadMajor');
    addListenerOnCheckbox('#triad-minor', 'triadMinor');
    addListenerOnCheckbox('#triad-diminished', 'triadDiminished');
    addListenerOnCheckbox('#seventh-major', 'seventhMajor');
    addListenerOnCheckbox('#seventh-minor', 'seventhMinor');
    addListenerOnCheckbox('#seventh-dominant', 'seventhDominant');
    addListenerOnCheckbox('#seventh-half-diminished', 'seventhHalfDiminished');
    addListenerOnCheckbox('#seventh-diminished', 'seventhDiminished');

    // keydown event
    document.addEventListener('keydown', e => {
      if (e.keyCode === 13) {
        // hit 'enter', start testing or assign new task
        e.preventDefault();
        let checkAnswerBtn = document.querySelector('#check-answer-btn');
        switch (checkAnswerBtn.innerHTML) {
          case CheckAnswerBtnState.Retry:
          case CheckAnswerBtnState.Go:
            let userInput = document.querySelector('#userAttempt-input');
            if (userInput.value !== '') {
              // simulate click checkAnswerBtn
              checkAnswerBtn.dispatchEvent(new Event('click'));
            }
            break;
          case CheckAnswerBtnState.Next:
            // simulate click checkAnswerBtn
            checkAnswerBtn.dispatchEvent(new Event('click'));
            break;
        }
      }
    });

    let userInput = document.querySelector('#userAttempt-input');
    userInput.addEventListener('input', e => {
      slashAutocomplete();
    });

    // keyboard btn

    let keyboardReturn = document.querySelector('#keyboardReturn');
    keyboardReturn.addEventListener('click', e => {
      let userInput = document.querySelector('#userAttempt-input');
      userInput.value = userInput.value.substr(0, userInput.value.length - 1);
      slashAutocomplete();
    });

    function addListenerOnKeyboardBtn(domID) {
      let domElement = document.querySelector(domID);
      domElement.addEventListener('click', e => {
        let userInput = document.querySelector('#userAttempt-input');
        userInput.value += domElement.innerHTML;
        slashAutocomplete();
      });
    }

    addListenerOnKeyboardBtn('#keyboardA');
    addListenerOnKeyboardBtn('#keyboardB');
    addListenerOnKeyboardBtn('#keyboardC');
    addListenerOnKeyboardBtn('#keyboardD');
    addListenerOnKeyboardBtn('#keyboardE');
    addListenerOnKeyboardBtn('#keyboardF');
    addListenerOnKeyboardBtn('#keyboardG');
    addListenerOnKeyboardBtn('#keyboardSharp');
    addListenerOnKeyboardBtn('#keyboardFlat');
  }

  addListener();
  assignNewTaskAndResetStyle();
})();
