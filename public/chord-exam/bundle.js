(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
const Interval = require('./Interval');
const Key_Name = Interval.Key_Name;
const Key_Signature_Name = Interval.Key_Signature_Name;

const Chord = require('./Chord');
const getNotesInChord = Chord.getNotesInChord;
const Chord_Table = Chord.Chord_Table;
const Chord_Name = Chord.Chord_Name;

function getRandomInteger(from, to) {
  const length = Math.abs(to - from) + 1;
  return Math.floor(Math.random() * length) + from;
}

function getChordInDemandRange(checkboxState) {
  let chordInDemand = [];

  if (checkboxState.triadMajor) {
    chordInDemand.push(Chord_Table.Triad_Major);
  }
  if (checkboxState.triadMinor) {
    chordInDemand.push(Chord_Table.Triad_Minor);
  }
  if (checkboxState.triadDiminished) {
    chordInDemand.push(Chord_Table.Triad_Diminished);
  }
  if (checkboxState.seventhMajor) {
    chordInDemand.push(Chord_Table.Seventh_Major);
  }
  if (checkboxState.seventhMinor) {
    chordInDemand.push(Chord_Table.Seventh_Minor);
  }
  if (checkboxState.seventhDominant) {
    chordInDemand.push(Chord_Table.Seventh_Dominant);
  }
  if (checkboxState.seventhHalfDiminished) {
    chordInDemand.push(Chord_Table.Seventh_Half_Diminished);
  }
  if (checkboxState.seventhDiminished) {
    chordInDemand.push(Chord_Table.Seventh_Diminished);
  }

  let idx = Math.floor(Math.random() * chordInDemand.length);

  return chordInDemand[idx];
}

/**
 *  @return {Object} - {baseKey, signature, chord}
 */
function getRandomQuestion(checkboxState) {
  // 七個白鍵
  const baseKeyIdx = getRandomInteger(0, 6);
  // 只考慮 還原、b、# 三種升降記號
  const signatureIdx = getRandomInteger(0, 2);
  // 根據使用者選擇要哪些和弦種類隨機取一個
  const chordIdx = getChordInDemandRange(checkboxState);

  return {
    baseKeyIdx, // correspond to Interval.Key
    signatureIdx, // correspond to Interval.Key_Signature
    chordIdx // correspond to Chord.Chord_Table
  };
}

function assignNewTask(checkboxState) {
  let infoObj = getRandomQuestion(checkboxState);

  updateQuestionKey(infoObj);
  return updateAnswer(infoObj);
}

function updateQuestionKey({ baseKeyIdx, signatureIdx, chordIdx }) {
  let baseKey = Key_Name[baseKeyIdx],
    signature = Key_Signature_Name[signatureIdx],
    chord = Chord_Name[chordIdx];

  let question = document.querySelector('#question-key');
  question.innerHTML = `
                        <span>${baseKey}
                            <sup>${signature}</sup>
                            <sub>${chord}</sub>
                        </span>`;
}

function updateAnswer({ baseKeyIdx, signatureIdx, chordIdx }) {
  let key = (baseKey = Key_Name[baseKeyIdx] + Key_Signature_Name[signatureIdx]),
    obj = getNotesInChord(key, chordIdx),
    notesInChord = obj.notesInChord,
    degreeArray = obj.degreeArray;

  let answer = document.querySelectorAll('.notes-in-key'),
    notesInAnswer = document.querySelectorAll('.notes-in-key-note'),
    degreeOfNotesInAnswer = document.querySelectorAll('.notes-in-key-degree');

  for (let i = 0; i < answer.length; i++) {
    updateInnerHTMLWithSup(notesInAnswer[i], notesInChord[i]);
    updateInnerHTMLWithSup(degreeOfNotesInAnswer[i], degreeArray[i]);
  }

  return notesInChord;
}

function updateInnerHTMLWithSup(element, word = '') {
  element.innerHTML = word;
}

module.exports = {
  assignNewTask
};

},{"./Chord":2,"./Interval":3}],2:[function(require,module,exports){
/**
 *  處理和絃
 *  在這裡要判斷出音程關係, interval.js 的狀態是已經知道音程關係, 然後找出相對音
 *
 */

const getNoteByInterval = require('./Interval').getNoteByInterval;

const Chord_Table = {
  Triad_Major: 0,
  Triad_Minor: 1,
  Triad_Diminished: 2,

  Seventh_Major: 3,
  Seventh_Minor: 4,
  Seventh_Dominant: 5,
  Seventh_Half_Diminished: 6,
  Seventh_Diminished: 7
};

const Chord_Name = {
  [Chord_Table.Triad_Major]: 'maj',
  [Chord_Table.Triad_Minor]: 'min',
  [Chord_Table.Triad_Diminished]: 'dim',
  [Chord_Table.Seventh_Major]: 'maj7',
  [Chord_Table.Seventh_Minor]: 'min7',
  [Chord_Table.Seventh_Dominant]: '7',
  [Chord_Table.Seventh_Half_Diminished]: 'm7b5',
  [Chord_Table.Seventh_Diminished]: 'dim7'
};

const Chord_Info = {
  [Chord_Table.Triad_Major]: {
    degree: ['1', '3', '5'],
    interval: [0, 4, 7]
  },
  [Chord_Table.Triad_Minor]: {
    degree: ['1', '3b', '5'],
    interval: [0, 3, 7]
  },
  [Chord_Table.Triad_Diminished]: {
    degree: ['1', '3b', '5b'],
    interval: [0, 3, 6]
  },

  [Chord_Table.Seventh_Major]: {
    degree: ['1', '3', '5', '7'],
    interval: [0, 4, 7, 11]
  },
  [Chord_Table.Seventh_Minor]: {
    degree: ['1', '3b', '5', '7b'],
    interval: [0, 3, 7, 10]
  },
  [Chord_Table.Seventh_Dominant]: {
    degree: ['1', '3', '5', '7b'],
    interval: [0, 4, 7, 10]
  },
  [Chord_Table.Seventh_Half_Diminished]: {
    degree: ['1', '3b', '5b', '7b'],
    interval: [0, 3, 6, 10]
  },
  [Chord_Table.Seventh_Diminished]: {
    degree: ['1', '3b', '5b', '7bb'],
    interval: [0, 3, 6, 9]
  }
};

/**
 *  @param {String} key    - eg: A, Eb, G#
 *  @param {Number} chordIdx - eg: maj7, dim7, min,...etc.
 *  #example
 *  getNotesInChord('A', Chord_Table.Seventh_Major)
 *
 *  return {
 *      notesInChord: ['A', 'C#', 'E', 'G#'],
 *      degreeArray: ['1', '3', '5', '7']
 *  }
 */
function getNotesInChord(key, chordIdx) {
  // G maj7
  const chordObj = Chord_Info[chordIdx],
    degreeArray = chordObj.degree,
    intervalArray = chordObj.interval;

  let notesInChord = [],
    degree,
    interval,
    note;

  for (let i = 0; i < degreeArray.length; i++) {
    degree = parseInt(degreeArray[i]);
    interval = intervalArray[i];
    note = getNoteByInterval(key, degree, interval);
    notesInChord.push(note);
  }

  return {
    notesInChord,
    degreeArray
  };
}

module.exports = {
  getNotesInChord,
  Chord_Table,
  Chord_Name
};

},{"./Interval":3}],3:[function(require,module,exports){
/**
 *  處理音程
 *
 *
 */

const Key_Amount = 7;
const Interval_Amount = 12;

const Key = {
  C: 0,
  D: 1,
  E: 2,
  F: 3,
  G: 4,
  A: 5,
  B: 6
};

const Key_Name = {
  [Key.C]: 'C',
  [Key.D]: 'D',
  [Key.E]: 'E',
  [Key.F]: 'F',
  [Key.G]: 'G',
  [Key.A]: 'A',
  [Key.B]: 'B'
};

const Key_Signature = {
  None: 0,
  Flat: 1,
  Sharp: 2
};

const Key_Signature_Name = {
  [Key_Signature.None]: '',
  [Key_Signature.Flat]: 'b',
  [Key_Signature.Sharp]: '#'
};

// 如果不夠的話再用 function
const Key_Interval_2D_Table = [
  [
    'C',
    'C#',
    'C##',
    'C###',
    'C####',
    'C#####',
    'Cbbbbbb',
    'Cbbbbb',
    'Cbbbb',
    'Cbbb',
    'Cbb',
    'Cb'
  ],
  [
    'Dbb',
    'Db',
    'D',
    'D#',
    'D##',
    'D###',
    'D####',
    'D#####',
    'Dbbbbbb',
    'Dbbbbb',
    'Dbbbb',
    'Dbbb'
  ],
  [
    'Ebbbb',
    'Ebbb',
    'Ebb',
    'Eb',
    'E',
    'E#',
    'E##',
    'E###',
    'E####',
    'E#####',
    'Ebbbbbb',
    'Ebbbbb'
  ],
  [
    'Fbbbbb',
    'Fbbbb',
    'Fbbb',
    'Fbb',
    'Fb',
    'F',
    'F#',
    'F##',
    'F###',
    'F####',
    'F#####',
    'Fbbbbbb'
  ],
  [
    'G#####',
    'Gbbbbbb',
    'Gbbbbb',
    'Gbbbb',
    'Gbbb',
    'Gbb',
    'Gb',
    'G',
    'G#',
    'G##',
    'G###',
    'G####'
  ],
  [
    'A###',
    'A####',
    'A#####',
    'Abbbbbb',
    'Abbbbb',
    'Abbbb',
    'Abbb',
    'Abb',
    'Ab',
    'A',
    'A#',
    'A##'
  ],
  [
    'B#',
    'B##',
    'B###',
    'B####',
    'B#####',
    'Bbbbbbb',
    'Bbbbbb',
    'Bbbbb',
    'Bbbb',
    'Bbb',
    'Bb',
    'B'
  ]
];

/**
 *
 *  @param {String} key - eg: G
 *  @return {Object} { keyIdx: 4, intervalIdx: 7}
 */
function _findIdxDegreeNInterval(key) {
  const keyIdx = Key[key[0].toUpperCase()],
    intervalIdx = Key_Interval_2D_Table[keyIdx].indexOf(key);

  return { keyIdx, intervalIdx };
}

/**
 *  getNoteByInterval(C, 3, 4) // E
 *  @param {String} baseNote - C
 *  @param {Number} degree - 3
 *  @param {Number} intervalDiff - 4
 *  @param {String} - E
 */
function getNoteByInterval(baseNote, degree, intervalDiff) {
  const o = _findIdxDegreeNInterval(baseNote),
    baseKeyIdx = o.keyIdx,
    baseIntervalIdx = o.intervalIdx;

  const outputKeyIdx = (baseKeyIdx + degree - 1) % Key_Amount,
    outputIntervalIdx = (baseIntervalIdx + intervalDiff) % Interval_Amount;

  const outputKey = Key_Interval_2D_Table[outputKeyIdx][outputIntervalIdx];

  return outputKey;
}

module.exports = {
  getNoteByInterval,
  Key_Name,
  Key_Signature_Name
};

},{}],4:[function(require,module,exports){
const assignNewTask = require('./app/AssignTask').assignNewTask;

const CheckAnswerBtnState = {
  Go: 'Correct？',
  Next: 'Next',
  Retry: 'Retry'
};

const ShowAnswerState = {
  Show: 'Answer',
  Hide: 'Hide'
};

const Reg = {
  Input: /[A-G][b|\#]?/, // allow 'B', 'C#', 'Fb'
  Key: /[A-G]/,
  Slash: /\//
};

const checkboxState = {
  triadMajor: true,
  triadMinor: true,
  triadDiminished: true,
  seventhMajor: true,
  seventhMinor: true,
  seventhDominant: true,
  seventhHalfDiminished: true,
  seventhDiminished: true
};

let currAnswer = [];

// DOM handler
(function() {
  /**
   *
   *  @param {String} inputString
   *  @return {Array}
   *  @example
   *  parseUserInput('A/C#/E/G')  // ['A', 'C#', 'E', 'G']
   */
  function parseUserInput(inputString) {
    // console.log(inputString);
    return inputString
      .trim()
      .split('/')
      .filter(char => Reg.Input.test(char));
  }

  function slashAutocomplete() {
    let userInput = document.querySelector('#userAttempt-input'),
      value = userInput.value.split('');
    if (value.length === 1) {
      return;
    }

    // try value.match(/[A-G]/g)
    let numOfKey = value.filter(char => Reg.Key.test(char)).length,
      numOfSlash = value.filter(char => Reg.Slash.test(char)).length,
      isLackOneSlashPattern = numOfKey - numOfSlash === 2, // eg: AC => A/C, G/BD=> G/B/D
      lastIndex = value.length - 1,
      lastChar = value[lastIndex],
      isLastCharKey = Reg.Key.test(lastChar);

    if (isLackOneSlashPattern && isLastCharKey) {
      userInput.value = userInput.value.substr(0, lastIndex) + '/' + lastChar;
    }
  }

  function showAnswer(toShow, btnInnerHTML) {
    let answerSection = document.querySelector('#question-answer');
    answerSection.style.display = toShow ? 'flex' : 'none';

    let getAnswerBtn = document.querySelector('#get-answer-btn');
    getAnswerBtn.innerHTML = btnInnerHTML;
  }

  function showSettingWindow(toShow) {
    let modal = document.querySelector('#myModal');
    modal.style.display = toShow ? 'block' : 'none';
  }

  function assignNewTaskAndResetStyle() {
    // hide answer
    showAnswer(false, ShowAnswerState.Show);
    // reset input value
    let userInput = document.querySelector('#userAttempt-input');
    userInput.value = '';
    userInput.classList.remove('wrong-answer');
    userInput.classList.remove('correct-answer');
    // reset btn
    let checkAnswerBtn = document.querySelector('#check-answer-btn');
    checkAnswerBtn.innerHTML = CheckAnswerBtnState.Go;
    // assign new task
    currAnswer = assignNewTask(checkboxState);
  }

  function addListener() {
    let checkAnswerBtn = document.querySelector('#check-answer-btn');
    checkAnswerBtn.addEventListener('click', () => {
      let userInput = document.querySelector('#userAttempt-input');
      switch (checkAnswerBtn.innerHTML) {
        case CheckAnswerBtnState.Go:
        case CheckAnswerBtnState.Retry:
          // check answer
          let inputInArray = parseUserInput(userInput.value),
            isCorrect =
              inputInArray.length === currAnswer.length &&
              currAnswer.every((char, i) => char === inputInArray[i]);

          if (isCorrect) {
            userInput.classList.remove('wrong-answer');
            userInput.classList.add('correct-answer');
            checkAnswerBtn.innerHTML = CheckAnswerBtnState.Next;
          } else {
            userInput.classList.remove('correct-answer');
            userInput.classList.add('wrong-answer');
            checkAnswerBtn.innerHTML = CheckAnswerBtnState.Retry;
          }
          break;
        case CheckAnswerBtnState.Next:
          assignNewTaskAndResetStyle();
          break;
        default:
          break;
      }
    });

    let getAnswerBtn = document.querySelector('#get-answer-btn');
    getAnswerBtn.addEventListener('click', () => {
      let toShow = getAnswerBtn.innerHTML === ShowAnswerState.Show,
        btnInnerHTML = toShow ? ShowAnswerState.Hide : ShowAnswerState.Show;
      showAnswer(toShow, btnInnerHTML);
    });

    let applySettingBtn = document.querySelector('#apply-setting-btn');
    applySettingBtn.addEventListener('click', () => {
      showSettingWindow(false);
      // whenever setting change, assign new task
      assignNewTaskAndResetStyle();
    });

    let settingBtn = document.querySelector('#setting-btn');
    settingBtn.addEventListener('click', () => showSettingWindow(true));

    // checkboxes setting

    // event delegation? but how?
    // let requirement = document.querySelector('#requirement');
    // requirement.addEventListener('click', () => {
    // });

    function addListenerOnCheckbox(domID, checkboxProperty) {
      let domElement = document.querySelector(domID);
      domElement.addEventListener('click', () => {
        // 要再檢查 checkboxState[checkboxProperty] 是不是目前唯一有選的, 要保證至少有一個 checkbox 打勾

        checkboxState[checkboxProperty] = domElement.checked;
      });
    }

    addListenerOnCheckbox('#triad-major', 'triadMajor');
    addListenerOnCheckbox('#triad-minor', 'triadMinor');
    addListenerOnCheckbox('#triad-diminished', 'triadDiminished');
    addListenerOnCheckbox('#seventh-major', 'seventhMajor');
    addListenerOnCheckbox('#seventh-minor', 'seventhMinor');
    addListenerOnCheckbox('#seventh-dominant', 'seventhDominant');
    addListenerOnCheckbox('#seventh-half-diminished', 'seventhHalfDiminished');
    addListenerOnCheckbox('#seventh-diminished', 'seventhDiminished');

    // keydown event
    document.addEventListener('keydown', e => {
      if (e.keyCode === 13) {
        // hit 'enter', start testing or assign new task
        e.preventDefault();
        let checkAnswerBtn = document.querySelector('#check-answer-btn');
        switch (checkAnswerBtn.innerHTML) {
          case CheckAnswerBtnState.Retry:
          case CheckAnswerBtnState.Go:
            let userInput = document.querySelector('#userAttempt-input');
            if (userInput.value !== '') {
              // simulate click checkAnswerBtn
              checkAnswerBtn.dispatchEvent(new Event('click'));
            }
            break;
          case CheckAnswerBtnState.Next:
            // simulate click checkAnswerBtn
            checkAnswerBtn.dispatchEvent(new Event('click'));
            break;
        }
      }
    });

    let userInput = document.querySelector('#userAttempt-input');
    userInput.addEventListener('input', e => {
      slashAutocomplete();
    });

    // keyboard btn

    let keyboardReturn = document.querySelector('#keyboardReturn');
    keyboardReturn.addEventListener('click', e => {
      let userInput = document.querySelector('#userAttempt-input');
      userInput.value = userInput.value.substr(0, userInput.value.length - 1);
      slashAutocomplete();
    });

    function addListenerOnKeyboardBtn(domID) {
      let domElement = document.querySelector(domID);
      domElement.addEventListener('click', e => {
        let userInput = document.querySelector('#userAttempt-input');
        userInput.value += domElement.innerHTML;
        slashAutocomplete();
      });
    }

    addListenerOnKeyboardBtn('#keyboardA');
    addListenerOnKeyboardBtn('#keyboardB');
    addListenerOnKeyboardBtn('#keyboardC');
    addListenerOnKeyboardBtn('#keyboardD');
    addListenerOnKeyboardBtn('#keyboardE');
    addListenerOnKeyboardBtn('#keyboardF');
    addListenerOnKeyboardBtn('#keyboardG');
    addListenerOnKeyboardBtn('#keyboardSharp');
    addListenerOnKeyboardBtn('#keyboardFlat');
  }

  addListener();
  assignNewTaskAndResetStyle();
})();

},{"./app/AssignTask":1}]},{},[4]);
