/**
 *  處理音程
 *
 *
 */

const Key_Amount = 7;
const Interval_Amount = 12;

const Key = {
  C: 0,
  D: 1,
  E: 2,
  F: 3,
  G: 4,
  A: 5,
  B: 6
};

const Key_Name = {
  [Key.C]: 'C',
  [Key.D]: 'D',
  [Key.E]: 'E',
  [Key.F]: 'F',
  [Key.G]: 'G',
  [Key.A]: 'A',
  [Key.B]: 'B'
};

const Key_Signature = {
  None: 0,
  Flat: 1,
  Sharp: 2
};

const Key_Signature_Name = {
  [Key_Signature.None]: '',
  [Key_Signature.Flat]: 'b',
  [Key_Signature.Sharp]: '#'
};

// 如果不夠的話再用 function
const Key_Interval_2D_Table = [
  [
    'C',
    'C#',
    'C##',
    'C###',
    'C####',
    'C#####',
    'Cbbbbbb',
    'Cbbbbb',
    'Cbbbb',
    'Cbbb',
    'Cbb',
    'Cb'
  ],
  [
    'Dbb',
    'Db',
    'D',
    'D#',
    'D##',
    'D###',
    'D####',
    'D#####',
    'Dbbbbbb',
    'Dbbbbb',
    'Dbbbb',
    'Dbbb'
  ],
  [
    'Ebbbb',
    'Ebbb',
    'Ebb',
    'Eb',
    'E',
    'E#',
    'E##',
    'E###',
    'E####',
    'E#####',
    'Ebbbbbb',
    'Ebbbbb'
  ],
  [
    'Fbbbbb',
    'Fbbbb',
    'Fbbb',
    'Fbb',
    'Fb',
    'F',
    'F#',
    'F##',
    'F###',
    'F####',
    'F#####',
    'Fbbbbbb'
  ],
  [
    'G#####',
    'Gbbbbbb',
    'Gbbbbb',
    'Gbbbb',
    'Gbbb',
    'Gbb',
    'Gb',
    'G',
    'G#',
    'G##',
    'G###',
    'G####'
  ],
  [
    'A###',
    'A####',
    'A#####',
    'Abbbbbb',
    'Abbbbb',
    'Abbbb',
    'Abbb',
    'Abb',
    'Ab',
    'A',
    'A#',
    'A##'
  ],
  [
    'B#',
    'B##',
    'B###',
    'B####',
    'B#####',
    'Bbbbbbb',
    'Bbbbbb',
    'Bbbbb',
    'Bbbb',
    'Bbb',
    'Bb',
    'B'
  ]
];

/**
 *
 *  @param {String} key - eg: G
 *  @return {Object} { keyIdx: 4, intervalIdx: 7}
 */
function _findIdxDegreeNInterval(key) {
  const keyIdx = Key[key[0].toUpperCase()],
    intervalIdx = Key_Interval_2D_Table[keyIdx].indexOf(key);

  return { keyIdx, intervalIdx };
}

/**
 *  getNoteByInterval(C, 3, 4) // E
 *  @param {String} baseNote - C
 *  @param {Number} degree - 3
 *  @param {Number} intervalDiff - 4
 *  @param {String} - E
 */
function getNoteByInterval(baseNote, degree, intervalDiff) {
  const o = _findIdxDegreeNInterval(baseNote),
    baseKeyIdx = o.keyIdx,
    baseIntervalIdx = o.intervalIdx;

  const outputKeyIdx = (baseKeyIdx + degree - 1) % Key_Amount,
    outputIntervalIdx = (baseIntervalIdx + intervalDiff) % Interval_Amount;

  const outputKey = Key_Interval_2D_Table[outputKeyIdx][outputIntervalIdx];

  return outputKey;
}

module.exports = {
  getNoteByInterval,
  Key_Name,
  Key_Signature_Name
};
