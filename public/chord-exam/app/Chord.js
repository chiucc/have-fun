/**
 *  處理和絃
 *  在這裡要判斷出音程關係, interval.js 的狀態是已經知道音程關係, 然後找出相對音
 *
 */

const getNoteByInterval = require('./Interval').getNoteByInterval;

const Chord_Table = {
  Triad_Major: 0,
  Triad_Minor: 1,
  Triad_Diminished: 2,

  Seventh_Major: 3,
  Seventh_Minor: 4,
  Seventh_Dominant: 5,
  Seventh_Half_Diminished: 6,
  Seventh_Diminished: 7
};

const Chord_Name = {
  [Chord_Table.Triad_Major]: 'maj',
  [Chord_Table.Triad_Minor]: 'min',
  [Chord_Table.Triad_Diminished]: 'dim',
  [Chord_Table.Seventh_Major]: 'maj7',
  [Chord_Table.Seventh_Minor]: 'min7',
  [Chord_Table.Seventh_Dominant]: '7',
  [Chord_Table.Seventh_Half_Diminished]: 'm7b5',
  [Chord_Table.Seventh_Diminished]: 'dim7'
};

const Chord_Info = {
  [Chord_Table.Triad_Major]: {
    degree: ['1', '3', '5'],
    interval: [0, 4, 7]
  },
  [Chord_Table.Triad_Minor]: {
    degree: ['1', '3b', '5'],
    interval: [0, 3, 7]
  },
  [Chord_Table.Triad_Diminished]: {
    degree: ['1', '3b', '5b'],
    interval: [0, 3, 6]
  },

  [Chord_Table.Seventh_Major]: {
    degree: ['1', '3', '5', '7'],
    interval: [0, 4, 7, 11]
  },
  [Chord_Table.Seventh_Minor]: {
    degree: ['1', '3b', '5', '7b'],
    interval: [0, 3, 7, 10]
  },
  [Chord_Table.Seventh_Dominant]: {
    degree: ['1', '3', '5', '7b'],
    interval: [0, 4, 7, 10]
  },
  [Chord_Table.Seventh_Half_Diminished]: {
    degree: ['1', '3b', '5b', '7b'],
    interval: [0, 3, 6, 10]
  },
  [Chord_Table.Seventh_Diminished]: {
    degree: ['1', '3b', '5b', '7bb'],
    interval: [0, 3, 6, 9]
  }
};

/**
 *  @param {String} key    - eg: A, Eb, G#
 *  @param {Number} chordIdx - eg: maj7, dim7, min,...etc.
 *  #example
 *  getNotesInChord('A', Chord_Table.Seventh_Major)
 *
 *  return {
 *      notesInChord: ['A', 'C#', 'E', 'G#'],
 *      degreeArray: ['1', '3', '5', '7']
 *  }
 */
function getNotesInChord(key, chordIdx) {
  // G maj7
  const chordObj = Chord_Info[chordIdx],
    degreeArray = chordObj.degree,
    intervalArray = chordObj.interval;

  let notesInChord = [],
    degree,
    interval,
    note;

  for (let i = 0; i < degreeArray.length; i++) {
    degree = parseInt(degreeArray[i]);
    interval = intervalArray[i];
    note = getNoteByInterval(key, degree, interval);
    notesInChord.push(note);
  }

  return {
    notesInChord,
    degreeArray
  };
}

module.exports = {
  getNotesInChord,
  Chord_Table,
  Chord_Name
};
