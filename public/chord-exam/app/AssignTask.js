const Interval = require('./Interval');
const Key_Name = Interval.Key_Name;
const Key_Signature_Name = Interval.Key_Signature_Name;

const Chord = require('./Chord');
const getNotesInChord = Chord.getNotesInChord;
const Chord_Table = Chord.Chord_Table;
const Chord_Name = Chord.Chord_Name;

function getRandomInteger(from, to) {
  const length = Math.abs(to - from) + 1;
  return Math.floor(Math.random() * length) + from;
}

function getChordInDemandRange(checkboxState) {
  let chordInDemand = [];

  if (checkboxState.triadMajor) {
    chordInDemand.push(Chord_Table.Triad_Major);
  }
  if (checkboxState.triadMinor) {
    chordInDemand.push(Chord_Table.Triad_Minor);
  }
  if (checkboxState.triadDiminished) {
    chordInDemand.push(Chord_Table.Triad_Diminished);
  }
  if (checkboxState.seventhMajor) {
    chordInDemand.push(Chord_Table.Seventh_Major);
  }
  if (checkboxState.seventhMinor) {
    chordInDemand.push(Chord_Table.Seventh_Minor);
  }
  if (checkboxState.seventhDominant) {
    chordInDemand.push(Chord_Table.Seventh_Dominant);
  }
  if (checkboxState.seventhHalfDiminished) {
    chordInDemand.push(Chord_Table.Seventh_Half_Diminished);
  }
  if (checkboxState.seventhDiminished) {
    chordInDemand.push(Chord_Table.Seventh_Diminished);
  }

  let idx = Math.floor(Math.random() * chordInDemand.length);

  return chordInDemand[idx];
}

/**
 *  @return {Object} - {baseKey, signature, chord}
 */
function getRandomQuestion(checkboxState) {
  // 七個白鍵
  const baseKeyIdx = getRandomInteger(0, 6);
  // 只考慮 還原、b、# 三種升降記號
  const signatureIdx = getRandomInteger(0, 2);
  // 根據使用者選擇要哪些和弦種類隨機取一個
  const chordIdx = getChordInDemandRange(checkboxState);

  return {
    baseKeyIdx, // correspond to Interval.Key
    signatureIdx, // correspond to Interval.Key_Signature
    chordIdx // correspond to Chord.Chord_Table
  };
}

function assignNewTask(checkboxState) {
  let infoObj = getRandomQuestion(checkboxState);

  updateQuestionKey(infoObj);
  return updateAnswer(infoObj);
}

function updateQuestionKey({ baseKeyIdx, signatureIdx, chordIdx }) {
  let baseKey = Key_Name[baseKeyIdx],
    signature = Key_Signature_Name[signatureIdx],
    chord = Chord_Name[chordIdx];

  let question = document.querySelector('#question-key');
  question.innerHTML = `
                        <span>${baseKey}
                            <sup>${signature}</sup>
                            <sub>${chord}</sub>
                        </span>`;
}

function updateAnswer({ baseKeyIdx, signatureIdx, chordIdx }) {
  let key = (baseKey = Key_Name[baseKeyIdx] + Key_Signature_Name[signatureIdx]),
    obj = getNotesInChord(key, chordIdx),
    notesInChord = obj.notesInChord,
    degreeArray = obj.degreeArray;

  let answer = document.querySelectorAll('.notes-in-key'),
    notesInAnswer = document.querySelectorAll('.notes-in-key-note'),
    degreeOfNotesInAnswer = document.querySelectorAll('.notes-in-key-degree');

  for (let i = 0; i < answer.length; i++) {
    updateInnerHTMLWithSup(notesInAnswer[i], notesInChord[i]);
    updateInnerHTMLWithSup(degreeOfNotesInAnswer[i], degreeArray[i]);
  }

  return notesInChord;
}

function updateInnerHTMLWithSup(element, word = '') {
  element.innerHTML = word;
}

module.exports = {
  assignNewTask
};
