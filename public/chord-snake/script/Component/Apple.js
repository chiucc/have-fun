const Position = require('./Position');

class Apple {
  constructor({ row, col, symbol = 'X' }) {
    this.pos = new Position({ row, col });
    this.symbol = symbol;
  }

  setPosition({ row, col }) {
    this.pos.setPos({ row, col });
  }
}

module.exports = Apple;
