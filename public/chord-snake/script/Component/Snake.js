const { GameMapWidth, GameMapLength, Direction } = require('../GameEnum');
const Position = require('./Position');

function getNextHeadPos(headPos, direction) {
  let rowDelta = 0;
  let colDelta = 0;

  switch (direction) {
    case Direction.Up:
      rowDelta = -1;
      colDelta = 0;
      break;
    case Direction.Down:
      rowDelta = 1;
      colDelta = 0;
      break;
    case Direction.Left:
      rowDelta = 0;
      colDelta = -1;
      break;
    case Direction.Right:
      rowDelta = 0;
      colDelta = 1;
      break;
    default:
      break;
  }

  // 要考慮蛇穿牆
  let nextHeadRow = headPos.row + rowDelta;
  let nextHeadCol = headPos.col + colDelta;

  if (nextHeadRow === GameMapWidth) {
    nextHeadRow = 0;
  } else if (nextHeadRow === -1) {
    nextHeadRow = GameMapWidth - 1;
  } else if (nextHeadCol === GameMapLength) {
    nextHeadCol = 0;
  } else if (nextHeadCol === -1) {
    nextHeadCol = GameMapLength - 1;
  }

  return new Position({ row: nextHeadRow, col: nextHeadCol });
}

const snakeLength = 3;

class Snake {
  constructor() {
    // snake = head + neck + tail
    this.headPos = new Position({
      row: Math.floor(GameMapWidth / 2) - 1,
      col: Math.floor(GameMapLength / 2) - 1,
    });
    this.direction = Direction.Right;
    this.prevBody = [];
    this.body = [this.headPos];

    this._initBody();
  }

  _initBody() {
    const length = snakeLength;
    for (let i = 1; i < length; i += 1) {
      const newBody = new Position({
        row: this.body[i - 1].row,
        col: this.body[i - 1].col - 1,
      });
      this.body.push(newBody);
    }
  }

  _update() {
    this.prevBody = this.body.slice();

    for (let i = this.body.length - 1; i > 0; i -= 1) {
      this.body[i] = this.body[i - 1];
    }
    this.body[0] = this.headPos;
  }

  move() {
    this.headPos = getNextHeadPos(this.headPos, this.direction);
    this._update();
  }

  turnDirection(direction) {
    this.direction = direction;
  }

  getBody() {
    return { prevBody: this.prevBody, body: this.body };
  }

  getNeckDirection() {
    const { row: headRow, col: headCol } = this.headPos;
    const { row: neckRow, col: neckCol } = this.body[1];

    const rowDelta = headRow - neckRow;
    const colDelta = headCol - neckCol;

    if (colDelta === 0) {
      if (rowDelta === 1 || rowDelta === -(GameMapWidth - 1)) {
        return Direction.Up;
      }
      if (rowDelta === -1 || rowDelta === GameMapWidth - 1) {
        return Direction.Down;
      }
    }
    if (rowDelta === 0) {
      if (colDelta === 1 || colDelta === -(GameMapLength - 1)) {
        return Direction.Left;
      }

      if (colDelta === -1 || colDelta === GameMapLength - 1) {
        return Direction.Right;
      }
    }

    throw new Error('Wrong Direction of Snake Neck');
  }
}

module.exports = Snake;
