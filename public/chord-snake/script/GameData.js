const Type = {
  Chord: 0,
  Scale: 1,
};

class GameData {
  constructor() {
    this.type = Type.Chord;
    this.task = []; // ['C', 'E', 'G', 'B']
    this.hint = []; // [1, 3, 5, 7]
    this.taskName = []; // ['C', '', 'maj7']

    this.score = 0;
    this.apples = [];
    // 用來檢查下一個可以吃的 apple 是誰
    this.nextSymbol = null;
    // 沒有照順序吃 apples 5次就結束遊戲
    this.healthPoints = 5;

    this.isEnd = true;
    this.isPause = true;
    this.fps = 4;
  }

  reset() {
    this.isEnd = false;
    this.score = 0;
    this.healthPoints = 5;
  }

  // type: chord, scale
  assignNewTask(notes, degrees, chordName) {
    this.task = notes;
    this.hint = degrees;
    this.taskName = chordName;

    this.apples.length = this.task.length;
    this.apples.fill(' ');

    // eslint-disable-next-line no-unused-vars
    const [firstSymbol, ...rest] = notes;
    this.nextSymbol = firstSymbol;
  }

  /**
   *
   * @param {Apple} apple
   */
  updateCurrApple(newApple) {
    const { symbol } = newApple;
    const idx = this.task.indexOf(symbol);
    this.apples[idx] = symbol;

    if (idx < this.task.length - 1) {
      this.nextSymbol = this.task[idx + 1];
    }
  }

  isFinishTask() {
    const sortedTask = this.task.slice().sort();
    const sortedApples = this.apples.slice().sort();
    return sortedTask.every((symbol, i) => symbol === sortedApples[i]);
  }

  isCorrectAppleEaten(appleEaten) {
    return appleEaten.symbol === this.nextSymbol;
  }

  updatePauseStatus(isPause) {
    this.isPause = isPause;
  }

  updateEndStatus(isEnd) {
    this.isEnd = isEnd;
  }

  updateScore(gainScore) {
    this.score += gainScore;
  }

  updateHealthPoints(loseHealthPoint) {
    this.healthPoints += loseHealthPoint;
    if (this.healthPoints === 0) {
      this.updateEndStatus(true);
    }
  }
}

const gameData = new GameData();

module.exports = gameData;
