const KeyAmount = 7;
const IntervalAmount = 12;

const Key = {
  C: 0,
  D: 1,
  E: 2,
  F: 3,
  G: 4,
  A: 5,
  B: 6,
};

const KeyName = {
  [Key.C]: 'C',
  [Key.D]: 'D',
  [Key.E]: 'E',
  [Key.F]: 'F',
  [Key.G]: 'G',
  [Key.A]: 'A',
  [Key.B]: 'B',
};

const KeySignature = {
  None: 0,
  Flat: 1,
  Sharp: 2,
};

const KeySignatureName = {
  [KeySignature.None]: '',
  [KeySignature.Flat]: 'b',
  [KeySignature.Sharp]: '#',
};

// TODO calculated by function
const KeyInterval2DTable = [
  ['C', 'C#', 'C##', 'C###', 'C####', 'C#####', 'Cbbbbbb', 'Cbbbbb', 'Cbbbb', 'Cbbb', 'Cbb', 'Cb'],
  ['Dbb', 'Db', 'D', 'D#', 'D##', 'D###', 'D####', 'D#####', 'Dbbbbbb', 'Dbbbbb', 'Dbbbb', 'Dbbb'],
  ['Ebbbb', 'Ebbb', 'Ebb', 'Eb', 'E', 'E#', 'E##', 'E###', 'E####', 'E#####', 'Ebbbbbb', 'Ebbbbb'],
  ['Fbbbbb', 'Fbbbb', 'Fbbb', 'Fbb', 'Fb', 'F', 'F#', 'F##', 'F###', 'F####', 'F#####', 'Fbbbbbb'],
  ['G#####', 'Gbbbbbb', 'Gbbbbb', 'Gbbbb', 'Gbbb', 'Gbb', 'Gb', 'G', 'G#', 'G##', 'G###', 'G####'],
  ['A###', 'A####', 'A#####', 'Abbbbbb', 'Abbbbb', 'Abbbb', 'Abbb', 'Abb', 'Ab', 'A', 'A#', 'A##'],
  ['B#', 'B##', 'B###', 'B####', 'B#####', 'Bbbbbbb', 'Bbbbbb', 'Bbbbb', 'Bbbb', 'Bbb', 'Bb', 'B'],
];

/**
 *
 *  @param {String} key - eg: G
 *  @return {Object} { keyIdx: 4, intervalIdx: 7}
 */
function findIdxDegreeNInterval(key) {
  const keyIdx = Key[key[0].toUpperCase()];
  const intervalIdx = KeyInterval2DTable[keyIdx].indexOf(key);

  return { keyIdx, intervalIdx };
}

/**
 *  getNoteByInterval(C, 3, 4) // E
 *  @param {String} baseNote - C
 *  @param {Number} degree - 3
 *  @param {Number} intervalDiff - 4
 *  @return {String} - E
 */
function getNoteByInterval(baseNote, degree, intervalDiff) {
  const { keyIdx: baseKeyIdx, intervalIdx: baseIntervalIdx } = findIdxDegreeNInterval(baseNote);

  const outputKeyIdx = (baseKeyIdx + degree - 1) % KeyAmount;
  const outputIntervalIdx = (baseIntervalIdx + intervalDiff) % IntervalAmount;

  const outputKey = KeyInterval2DTable[outputKeyIdx][outputIntervalIdx];

  return outputKey;
}

module.exports = {
  getNoteByInterval,
  KeyName,
  KeySignatureName,
};
