const { getNoteByInterval } = require('./Interval');
const { getRandomInteger } = require('../utils/MathUtility');

const ChordType = {
  TriadMajor: 'maj',
  TriadMinor: 'min',
  TriadDiminished: 'dim',

  SeventhMajor: 'maj7',
  SeventhMinor: 'min7',
  SeventhDominant: '7',
  SeventhHalfDiminished: 'm7b5',
  SeventhDiminished: 'dim7',
};

const ChordInfo = {
  [ChordType.TriadMajor]: {
    degrees: ['1', '3', '5'],
    intervals: [0, 4, 7],
  },
  [ChordType.TriadMinor]: {
    degrees: ['1', '3b', '5'],
    intervals: [0, 3, 7],
  },
  [ChordType.TriadDiminished]: {
    degrees: ['1', '3b', '5b'],
    intervals: [0, 3, 6],
  },

  [ChordType.SeventhMajor]: {
    degrees: ['1', '3', '5', '7'],
    intervals: [0, 4, 7, 11],
  },
  [ChordType.SeventhMinor]: {
    degrees: ['1', '3b', '5', '7b'],
    intervals: [0, 3, 7, 10],
  },
  [ChordType.SeventhDominant]: {
    degrees: ['1', '3', '5', '7b'],
    intervals: [0, 4, 7, 10],
  },
  [ChordType.SeventhHalfDiminished]: {
    degrees: ['1', '3b', '5b', '7b'],
    intervals: [0, 3, 6, 10],
  },
  [ChordType.SeventhDiminished]: {
    degrees: ['1', '3b', '5b', '7bb'],
    intervals: [0, 3, 6, 9],
  },
};

/**
 *  @param {String} key    - eg: A, Eb, G#
 *  @param {Number} chordType - eg: maj7, dim7, min,...etc.
 *  #example
 *  getNotesInChord('A', ChordType.Seventh_Major)
 *
 *  return {
 *      notesInChord: ['A', 'C#', 'E', 'G#'],
 *      degrees: ['1', '3', '5', '7']
 *  }
 */
function getNotesInChord(key, chordType) {
  // G maj7
  const { degrees, intervals } = ChordInfo[chordType];

  const notesInChord = [];
  let degree;
  let interval;
  let note;

  for (let i = 0; i < degrees.length; i += 1) {
    degree = parseInt(degrees[i], 10);
    interval = intervals[i];
    note = getNoteByInterval(key, degree, interval);
    notesInChord.push(note);
  }

  return {
    notesInChord,
    degrees,
  };
}

function getRandomChordType() {
  const chordTypes = Object.values(ChordType);
  const randIdx = getRandomInteger(0, chordTypes.length - 1);
  return chordTypes[randIdx];
}

module.exports = {
  ChordType,
  getNotesInChord,
  getRandomChordType,
};
