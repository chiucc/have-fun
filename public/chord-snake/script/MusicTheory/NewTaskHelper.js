const { getNotesInChord, getRandomChordType } = require('./Chord');
const { KeyName, KeySignatureName } = require('./Interval');
const { getRandomInteger } = require('../utils/MathUtility');

/**
 *  @return {Object} - {baseKey, signature, chordType}
 */
function getRandomQuestion() {
  // 七個白鍵
  const baseKeyIdx = getRandomInteger(0, 6);
  // 只考慮 還原、b、# 三種升降記號
  const signatureIdx = getRandomInteger(0, 2);
  // 根據使用者選擇要哪些和弦種類隨機取一個
  const chordType = getRandomChordType();

  return {
    baseKey: KeyName[baseKeyIdx],
    signature: KeySignatureName[signatureIdx],
    chordType,
  };
}

function assignNewTask() {
  const { baseKey, signature, chordType } = getRandomQuestion();
  const key = baseKey + signature;
  const { notesInChord, degrees } = getNotesInChord(key, chordType);
  const chordName = [baseKey, signature, chordType];

  return { notesInChord, degrees, chordName };
}

module.exports = {
  assignNewTask,
};
