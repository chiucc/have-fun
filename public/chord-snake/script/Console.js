const GameData = require('./GameData');
const { createElementWithClass } = require('./utils/DOMUtility');

function _assignTaskText(dataArr, parentEle, className) {
  const childrenElementLength = parentEle.children.length;
  if (childrenElementLength) {
    const childrenEles = parentEle.children;
    const lengthDiff = dataArr.length - childrenElementLength;

    if (lengthDiff > 0) {
      for (let i = 0; i < lengthDiff; i += 1) {
        createElementWithClass(parentEle, className);
      }
    } else if (lengthDiff < 0) {
      for (let i = 0; i < -lengthDiff; i += 1) {
        childrenEles[childrenElementLength - 1 - i].classList.add('hide');
      }
    }

    dataArr.forEach((data, i) => {
      childrenEles[i].classList.remove('hide');
      childrenEles[i].innerHTML = data;
    });
  } else {
    dataArr.forEach((data) => {
      const node = createElementWithClass(parentEle, className);
      node.innerHTML = data;
    });
  }
}

function _updateTask() {
  const task = document.querySelector('#taskName');
  const [baseKey, signature, chordType] = GameData.taskName;
  task.innerHTML = `
                    <span>${baseKey}
                        <sup>${signature}</sup>
                        <sub>${chordType}</sub>
                    </span>`;
}

function _updateHint() {
  const hintDataArr = GameData.hint;
  const hintEle = document.querySelector('#hint');
  const className = 'task-symbol';

  _assignTaskText(hintDataArr, hintEle, className);
}

function updateInProgress() {
  const applesDataArr = GameData.apples;
  const applesEle = document.querySelector('#apples');
  const className = 'task-symbol';

  _assignTaskText(applesDataArr, applesEle, className);
}

function _updateScore() {
  const scoreEle = document.querySelector('#score');
  scoreEle.innerHTML = GameData.score;
}

function updateHealthPoints() {
  const hpEle = document.querySelector('#health-points');
  hpEle.innerHTML = GameData.healthPoints;
}

function updatePauseBtnText() {
  const pauseBtn = document.querySelector('#pause-btn');
  pauseBtn.innerHTML = GameData.isPause ? 'Resume' : 'Pause';
}

function updateBtnWording() {
  const { isEnd } = GameData;
  const startBtn = document.querySelector('#start-btn');
  const pauseBtn = document.querySelector('#pause-btn');
  startBtn.disabled = !isEnd;
  pauseBtn.disabled = isEnd;
}

function closeGameMap(isClose) {
  const gameMap = document.querySelector('#game-map');
  if (isClose) {
    gameMap.classList.add('close');
  } else {
    gameMap.classList.remove('close');
  }
}

function assignNewTask() {
  _updateTask();
  _updateHint();
  updateInProgress();
  _updateScore();
  updateHealthPoints();
}

module.exports = {
  assignNewTask,
  updateInProgress,
  updatePauseBtnText,
  updateHealthPoints,
  updateBtnWording,
  closeGameMap,
};
