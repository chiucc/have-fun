const Direction = {
  Up: 0,
  Down: 1,
  Left: 2,
  Right: 3,
};

const GameMapLength = 8;
const GameMapWidth = 8;

module.exports = {
  Direction,
  GameMapLength,
  GameMapWidth,
};
