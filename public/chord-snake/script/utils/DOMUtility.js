function createElementWithClass(parent, className) {
  const node = document.createElement('div');
  parent.appendChild(node);
  node.classList.add(className);
  return node;
}

function drawDOM(gameMap) {
  const gameMapEle = document.querySelector('#game-map');

  const rowLength = gameMap.length;
  const colLength = gameMap[0].length;

  if (gameMapEle.children.length === 0) {
    for (let i = 0; i < rowLength; i += 1) {
      for (let j = 0; j < colLength; j += 1) {
        const node = createElementWithClass(gameMapEle, 'block');
        const gameMapData = gameMap[i][j];
        const isPath = gameMapData === 0;
        const isSnake = gameMapData === 1;
        const isApple = !isPath && !isSnake;
        const text = !isApple ? '' : gameMapData;

        node.innerHTML = text;

        if (!isPath) {
          const className = isSnake ? 'snake' : 'apple';
          node.classList.add(className);
        }
      }
    }
  } else {
    for (let i = 0; i < rowLength; i += 1) {
      for (let j = 0; j < colLength; j += 1) {
        const node = gameMapEle.children[i * colLength + j];
        node.className = 'block';
        const gameMapData = gameMap[i][j];
        const isPath = gameMapData === 0;
        const isSnake = gameMapData === 1;
        const isApple = !isPath && !isSnake;
        const text = !isApple ? '' : gameMapData;

        node.innerHTML = text;

        if (!isPath) {
          const className = isSnake ? 'snake' : 'apple';
          node.classList.add(className);
        }
      }
    }
  }
}

module.exports = {
  createElementWithClass,
  drawDOM,
};
