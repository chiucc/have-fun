const { GameMapWidth, GameMapLength } = require('./GameEnum');
const GameData = require('./GameData');
const Position = require('./Component/Position');
const Snake = require('./Component/Snake');
const Apple = require('./Component/Apple');
const { getRandomInteger } = require('./utils/MathUtility');

function isDirectionValid(direction, snakeBodyHold) {
  return direction !== snakeBodyHold;
}

function print(gameMap) {
  const e = new CustomEvent('UpdateGameMapOnDOM', {
    detail: {
      gameMap,
    },
  });
  window.dispatchEvent(e);

  let str = '';
  gameMap.forEach((subArr) => {
    str += `${subArr.join(' ')}\n`;
  });
  console.log(str);
}

function getValidPos(currOcupiedPos) {
  let isInvalid = true;
  let randRow = null;
  let randCol = null;

  do {
    randRow = getRandomInteger(0, GameMapWidth - 1);
    randCol = getRandomInteger(0, GameMapLength - 1);
    isInvalid = currOcupiedPos.some(pos => pos.row === randRow && pos.col === randCol);
  } while (isInvalid);

  return { row: randRow, col: randCol };
}

class GameLogic {
  constructor() {
    this.gameMap = [];
    this.snake = new Snake();
    this.apples = [];
    this._initGameMap();
  }

  reset() {
    for (let i = 0; i < GameMapWidth; i += 1) {
      for (let j = 0; j < GameMapLength; j += 1) {
        this.gameMap[i][j] = 0;
      }
    }

    this.snake = new Snake();
    this.apples.length = 0;

    this._addSnakeOnGameMap();
  }

  _initGameMap() {
    this.gameMap = new Array(GameMapWidth);
    for (let i = 0; i < GameMapWidth; i += 1) {
      this.gameMap[i] = new Array(GameMapLength);
      for (let j = 0; j < GameMapLength; j += 1) {
        this.gameMap[i][j] = 0;
      }
    }
  }

  _checkEatApple(snakeHeadPos) {
    let appleEaten = null;
    this.apples.some((apple) => {
      if (snakeHeadPos.equal(apple.pos)) {
        appleEaten = apple;
        return true;
      }
      return false;
    });
    return appleEaten;
  }

  _addSnakeOnGameMap() {
    const { prevBody: prevSnake, body: newSnake } = this.snake.getBody();

    this._removePrevSnakeOnGameMap(prevSnake);

    const appleEaten = this._checkEatApple(newSnake[0]);
    if (appleEaten !== null) {
      this._removeAppleInDataArray(appleEaten);
      this._removeAppleOnGameMap(appleEaten);

      if (!GameData.isCorrectAppleEaten(appleEaten)) {
        // 重新產生一個被吃掉的 apple 到場景上
        this.reassignApple(appleEaten);

        // problem: 如果用 event 傳去 index 再更新 GameData, 可能會來不及更新 isEnd 狀態
        GameData.updateHealthPoints(-1);
        window.dispatchEvent(new CustomEvent('UpdateHealthPoints'));

        if (GameData.isEnd) {
          window.dispatchEvent(new CustomEvent('EndOfGame'));
        }
      } else {
        const eventEatApple = new CustomEvent('SnakeEatApple', {
          detail: {
            appleEaten,
          },
        });

        window.dispatchEvent(eventEatApple);

        if (GameData.isFinishTask()) {
          window.dispatchEvent(new CustomEvent('UpdateScore'));
          window.dispatchEvent(new CustomEvent('Pause'));
          setTimeout(() => {
            window.dispatchEvent(new CustomEvent('NextRound'));
          }, 500);
        }
      }
    }

    this._addNewSnakeOnGameMap(newSnake);
    print(this.gameMap);
  }

  _removePrevSnakeOnGameMap(prevSnake) {
    prevSnake.forEach((pos) => {
      this.gameMap[pos.row][pos.col] = 0;
    });
  }

  _addNewSnakeOnGameMap(newSnake) {
    newSnake.forEach((pos) => {
      this.gameMap[pos.row][pos.col] = 1;
    });
  }

  _addAppleOnGameMap() {
    this.apples.forEach((apple) => {
      this.gameMap[apple.pos.row][apple.pos.col] = apple.symbol;
    });
  }

  _removeAppleOnGameMap(apple) {
    this.gameMap[apple.pos.row][apple.pos.col] = 0;
  }

  _removeAppleInDataArray(appleEaten) {
    this.apples = this.apples.filter(apple => apple.symbol !== appleEaten.symbol);
  }

  moveSnake() {
    this.snake.move();
    this._addSnakeOnGameMap();
  }

  turnSnakeDirection(direction) {
    // 如果蛇正往右走，不能按 left，在這裡擋掉
    if (!isDirectionValid(direction, this.snake.getNeckDirection())) {
      return;
    }
    this.snake.turnDirection(direction);
  }

  reassignApple(apple) {
    const { body } = this.snake.getBody();
    const currOcupiedPos = [...body.slice(), ...this.apples.map(a => a.pos)];
    const { row, col } = getValidPos(currOcupiedPos);

    this.apples.push(new Apple({ row, col, symbol: apple.symbol }));

    this._addAppleOnGameMap();
    print(this.gameMap);
  }

  assignNewTask() {
    const { body } = this.snake.getBody();
    const currOcupiedPos = body.slice();

    this.apples = GameData.task.map((note) => {
      const { row, col } = getValidPos(currOcupiedPos);
      currOcupiedPos.push(new Position({ row, col }));
      return new Apple({ row, col, symbol: note });
    });

    this._addAppleOnGameMap();
    print(this.gameMap);
  }
}

const gameLogic = new GameLogic();

module.exports = gameLogic;
