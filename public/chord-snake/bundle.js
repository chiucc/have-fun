(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
const { Direction } = require('./script/GameEnum');
const GameData = require('./script/GameData');
const GameLogic = require('./script/GameLogic');
const Console = require('./script/Console');
const NewTaskHelper = require('./script/MusicTheory/NewTaskHelper');
const AnimationControl = require('./script/AnimationControl');
const { drawDOM } = require('./script/utils/DOMUtility');

function makeSureRequestAnimationFrame() {
  const requestAnimationFrame = window.requestAnimationFrame
    || window.mozRequestAnimationFrame
    || window.webkitRequestAnimationFrame
    || window.msRequestAnimationFrame;
  window.requestAnimationFrame = requestAnimationFrame;
}

function assignNewTask() {
  const { notesInChord: notes, degrees, chordName } = NewTaskHelper.assignNewTask();

  GameData.assignNewTask(notes, degrees, chordName);
  Console.assignNewTask();
  GameLogic.assignNewTask();

  GameData.updatePauseStatus(false);
  AnimationControl.setStop(false);
  AnimationControl.resume();
}

function startGame() {
  GameLogic.reset();
  GameData.reset();
  AnimationControl.setStop(false);

  const { fps } = GameData;
  const initialCallback = assignNewTask;
  const everyFrameCallbcak = GameLogic.moveSnake.bind(GameLogic);

  AnimationControl.setEveryFrameCallbcak(everyFrameCallbcak);
  AnimationControl.startAnimating(fps, initialCallback);

  Console.updateBtnWording();
  Console.closeGameMap(false);
}

function togglePause() {
  const togglePauseStatus = !GameData.isPause;
  GameData.updatePauseStatus(togglePauseStatus);
  Console.updatePauseBtnText();

  AnimationControl.setStop(togglePauseStatus);
  if (!togglePauseStatus) {
    AnimationControl.resume();
  }
}

function addListener() {
  window.addEventListener('NextRound', () => {
    assignNewTask();
  });

  window.addEventListener('Pause', () => {
    GameData.updatePauseStatus(true);
    AnimationControl.setStop(true);
    AnimationControl.resume();
  });

  window.addEventListener('SnakeEatApple', (e) => {
    const { appleEaten } = e.detail;
    GameData.updateCurrApple(appleEaten);
    Console.updateInProgress();
  });

  window.addEventListener('UpdateScore', () => {
    const gainScore = 1;
    GameData.updateScore(gainScore);
  });

  window.addEventListener('UpdateHealthPoints', () => {
    Console.updateHealthPoints();
  });

  window.addEventListener('UpdateGameMapOnDOM', (e) => {
    const { gameMap } = e.detail;
    drawDOM(gameMap);
  });

  window.addEventListener('EndOfGame', () => {
    console.log('End Of Game!');

    GameData.updateEndStatus(true);
    AnimationControl.setStop(true);
    Console.updateBtnWording();
    Console.closeGameMap(true);
  });

  document.addEventListener('keydown', (e) => {
    const { keyCode } = e;
    e.preventDefault();

    if (GameData.isPause || GameData.isEnd) {
      return;
    }

    switch (keyCode) {
      case 37: // left arrow
        GameLogic.turnSnakeDirection(Direction.Left);
        break;
      case 39: // right arrow
        GameLogic.turnSnakeDirection(Direction.Right);
        break;
      case 40: // down arrow
        GameLogic.turnSnakeDirection(Direction.Down);
        break;
      case 38: // up arrow
        GameLogic.turnSnakeDirection(Direction.Up);
        break;
      default:
        break;
    }
  });

  document.querySelector('#start-btn').addEventListener('click', () => {
    if (GameData.isEnd) {
      startGame();
    }
  });

  document.querySelector('#pause-btn').addEventListener('click', () => {
    if (!GameData.isEnd) {
      togglePause();
    }
  });
}

makeSureRequestAnimationFrame();
addListener();
Console.updateBtnWording();

},{"./script/AnimationControl":2,"./script/Console":6,"./script/GameData":7,"./script/GameEnum":8,"./script/GameLogic":9,"./script/MusicTheory/NewTaskHelper":12,"./script/utils/DOMUtility":13}],2:[function(require,module,exports){
/*
    for animation
    ref: https://stackoverflow.com/a/19772220
         http://jsfiddle.net/m1erickson/CtsY3/
*/
class AnaimationControl {
  constructor() {
    this.fps = null;
    this.fpsInterval = null;
    this.startTime = null;
    this.now = null;
    this.then = null;
    this.elapsed = null;

    this.isStop = false;

    this.everyFrameCallback = null;
  }

  setStop(isStop) {
    this.isStop = isStop;
  }

  setEveryFrameCallbcak(everyFrameCallback) {
    this.everyFrameCallback = everyFrameCallback;
  }

  startAnimating(fps = 30, initialCallback) {
    this.fps = fps;
    this.fpsInterval = 1000 / this.fps;
    this.then = Date.now();
    this.startTime = this.then;

    console.log(` startAnimating, startTime: ${this.startTime}`);

    // GameLogic.assignNewBlock();
    initialCallback && initialCallback();
    this._animate();
  }

  resume() {
    this._animate();
  }

  _animate() {
    if (this.isStop) {
      return;
    }

    // request another frame
    window.requestAnimationFrame(this._animate.bind(this));

    // calculate elapsed time since last loop
    this.now = Date.now();
    this.elapsed = this.now - this.then;

    // if enough time has elapsed, draw the next frame
    if (this.elapsed > this.fpsInterval) {
      // Get ready for next frame by setting then=now, but...
      // Also, adjust for fpsInterval not being multiple of 16.67
      this.then = this.now - (this.elapsed % this.fpsInterval);

      // draw stuff here
      // GameLogic.moveBlock(MoveDirection.Down);
      this.everyFrameCallback && this.everyFrameCallback();
    }
  }
}

const animationControl = new AnaimationControl();

module.exports = animationControl;

},{}],3:[function(require,module,exports){
const Position = require('./Position');

class Apple {
  constructor({ row, col, symbol = 'X' }) {
    this.pos = new Position({ row, col });
    this.symbol = symbol;
  }

  setPosition({ row, col }) {
    this.pos.setPos({ row, col });
  }
}

module.exports = Apple;

},{"./Position":4}],4:[function(require,module,exports){
class Position {
  constructor({ row = -1, col = -1 } = {}) {
    this.setPos({ row, col });
  }

  setPos({ row, col }) {
    this.row = row;
    this.col = col;
  }

  updatePosByDelta({ rowDelta, colDelta }) {
    this.row += rowDelta;
    this.col += colDelta;
  }

  reset() {
    this.setPos({ row: -1, col: -1 });
  }

  equal(other) {
    return this.row === other.row && this.col === other.col;
  }
}

module.exports = Position;

},{}],5:[function(require,module,exports){
const { GameMapWidth, GameMapLength, Direction } = require('../GameEnum');
const Position = require('./Position');

function getNextHeadPos(headPos, direction) {
  let rowDelta = 0;
  let colDelta = 0;

  switch (direction) {
    case Direction.Up:
      rowDelta = -1;
      colDelta = 0;
      break;
    case Direction.Down:
      rowDelta = 1;
      colDelta = 0;
      break;
    case Direction.Left:
      rowDelta = 0;
      colDelta = -1;
      break;
    case Direction.Right:
      rowDelta = 0;
      colDelta = 1;
      break;
    default:
      break;
  }

  // 要考慮蛇穿牆
  let nextHeadRow = headPos.row + rowDelta;
  let nextHeadCol = headPos.col + colDelta;

  if (nextHeadRow === GameMapWidth) {
    nextHeadRow = 0;
  } else if (nextHeadRow === -1) {
    nextHeadRow = GameMapWidth - 1;
  } else if (nextHeadCol === GameMapLength) {
    nextHeadCol = 0;
  } else if (nextHeadCol === -1) {
    nextHeadCol = GameMapLength - 1;
  }

  return new Position({ row: nextHeadRow, col: nextHeadCol });
}

const snakeLength = 3;

class Snake {
  constructor() {
    // snake = head + neck + tail
    this.headPos = new Position({
      row: Math.floor(GameMapWidth / 2) - 1,
      col: Math.floor(GameMapLength / 2) - 1,
    });
    this.direction = Direction.Right;
    this.prevBody = [];
    this.body = [this.headPos];

    this._initBody();
  }

  _initBody() {
    const length = snakeLength;
    for (let i = 1; i < length; i += 1) {
      const newBody = new Position({
        row: this.body[i - 1].row,
        col: this.body[i - 1].col - 1,
      });
      this.body.push(newBody);
    }
  }

  _update() {
    this.prevBody = this.body.slice();

    for (let i = this.body.length - 1; i > 0; i -= 1) {
      this.body[i] = this.body[i - 1];
    }
    this.body[0] = this.headPos;
  }

  move() {
    this.headPos = getNextHeadPos(this.headPos, this.direction);
    this._update();
  }

  turnDirection(direction) {
    this.direction = direction;
  }

  getBody() {
    return { prevBody: this.prevBody, body: this.body };
  }

  getNeckDirection() {
    const { row: headRow, col: headCol } = this.headPos;
    const { row: neckRow, col: neckCol } = this.body[1];

    const rowDelta = headRow - neckRow;
    const colDelta = headCol - neckCol;

    if (colDelta === 0) {
      if (rowDelta === 1 || rowDelta === -(GameMapWidth - 1)) {
        return Direction.Up;
      }
      if (rowDelta === -1 || rowDelta === GameMapWidth - 1) {
        return Direction.Down;
      }
    }
    if (rowDelta === 0) {
      if (colDelta === 1 || colDelta === -(GameMapLength - 1)) {
        return Direction.Left;
      }

      if (colDelta === -1 || colDelta === GameMapLength - 1) {
        return Direction.Right;
      }
    }

    throw new Error('Wrong Direction of Snake Neck');
  }
}

module.exports = Snake;

},{"../GameEnum":8,"./Position":4}],6:[function(require,module,exports){
const GameData = require('./GameData');
const { createElementWithClass } = require('./utils/DOMUtility');

function _assignTaskText(dataArr, parentEle, className) {
  const childrenElementLength = parentEle.children.length;
  if (childrenElementLength) {
    const childrenEles = parentEle.children;
    const lengthDiff = dataArr.length - childrenElementLength;

    if (lengthDiff > 0) {
      for (let i = 0; i < lengthDiff; i += 1) {
        createElementWithClass(parentEle, className);
      }
    } else if (lengthDiff < 0) {
      for (let i = 0; i < -lengthDiff; i += 1) {
        childrenEles[childrenElementLength - 1 - i].classList.add('hide');
      }
    }

    dataArr.forEach((data, i) => {
      childrenEles[i].classList.remove('hide');
      childrenEles[i].innerHTML = data;
    });
  } else {
    dataArr.forEach((data) => {
      const node = createElementWithClass(parentEle, className);
      node.innerHTML = data;
    });
  }
}

function _updateTask() {
  const task = document.querySelector('#taskName');
  const [baseKey, signature, chordType] = GameData.taskName;
  task.innerHTML = `
                    <span>${baseKey}
                        <sup>${signature}</sup>
                        <sub>${chordType}</sub>
                    </span>`;
}

function _updateHint() {
  const hintDataArr = GameData.hint;
  const hintEle = document.querySelector('#hint');
  const className = 'task-symbol';

  _assignTaskText(hintDataArr, hintEle, className);
}

function updateInProgress() {
  const applesDataArr = GameData.apples;
  const applesEle = document.querySelector('#apples');
  const className = 'task-symbol';

  _assignTaskText(applesDataArr, applesEle, className);
}

function _updateScore() {
  const scoreEle = document.querySelector('#score');
  scoreEle.innerHTML = GameData.score;
}

function updateHealthPoints() {
  const hpEle = document.querySelector('#health-points');
  hpEle.innerHTML = GameData.healthPoints;
}

function updatePauseBtnText() {
  const pauseBtn = document.querySelector('#pause-btn');
  pauseBtn.innerHTML = GameData.isPause ? 'Resume' : 'Pause';
}

function updateBtnWording() {
  const { isEnd } = GameData;
  const startBtn = document.querySelector('#start-btn');
  const pauseBtn = document.querySelector('#pause-btn');
  startBtn.disabled = !isEnd;
  pauseBtn.disabled = isEnd;
}

function closeGameMap(isClose) {
  const gameMap = document.querySelector('#game-map');
  if (isClose) {
    gameMap.classList.add('close');
  } else {
    gameMap.classList.remove('close');
  }
}

function assignNewTask() {
  _updateTask();
  _updateHint();
  updateInProgress();
  _updateScore();
  updateHealthPoints();
}

module.exports = {
  assignNewTask,
  updateInProgress,
  updatePauseBtnText,
  updateHealthPoints,
  updateBtnWording,
  closeGameMap,
};

},{"./GameData":7,"./utils/DOMUtility":13}],7:[function(require,module,exports){
const Type = {
  Chord: 0,
  Scale: 1,
};

class GameData {
  constructor() {
    this.type = Type.Chord;
    this.task = []; // ['C', 'E', 'G', 'B']
    this.hint = []; // [1, 3, 5, 7]
    this.taskName = []; // ['C', '', 'maj7']

    this.score = 0;
    this.apples = [];
    // 用來檢查下一個可以吃的 apple 是誰
    this.nextSymbol = null;
    // 沒有照順序吃 apples 5次就結束遊戲
    this.healthPoints = 5;

    this.isEnd = true;
    this.isPause = true;
    this.fps = 4;
  }

  reset() {
    this.isEnd = false;
    this.score = 0;
    this.healthPoints = 5;
  }

  // type: chord, scale
  assignNewTask(notes, degrees, chordName) {
    this.task = notes;
    this.hint = degrees;
    this.taskName = chordName;

    this.apples.length = this.task.length;
    this.apples.fill(' ');

    // eslint-disable-next-line no-unused-vars
    const [firstSymbol, ...rest] = notes;
    this.nextSymbol = firstSymbol;
  }

  /**
   *
   * @param {Apple} apple
   */
  updateCurrApple(newApple) {
    const { symbol } = newApple;
    const idx = this.task.indexOf(symbol);
    this.apples[idx] = symbol;

    if (idx < this.task.length - 1) {
      this.nextSymbol = this.task[idx + 1];
    }
  }

  isFinishTask() {
    const sortedTask = this.task.slice().sort();
    const sortedApples = this.apples.slice().sort();
    return sortedTask.every((symbol, i) => symbol === sortedApples[i]);
  }

  isCorrectAppleEaten(appleEaten) {
    return appleEaten.symbol === this.nextSymbol;
  }

  updatePauseStatus(isPause) {
    this.isPause = isPause;
  }

  updateEndStatus(isEnd) {
    this.isEnd = isEnd;
  }

  updateScore(gainScore) {
    this.score += gainScore;
  }

  updateHealthPoints(loseHealthPoint) {
    this.healthPoints += loseHealthPoint;
    if (this.healthPoints === 0) {
      this.updateEndStatus(true);
    }
  }
}

const gameData = new GameData();

module.exports = gameData;

},{}],8:[function(require,module,exports){
const Direction = {
  Up: 0,
  Down: 1,
  Left: 2,
  Right: 3,
};

const GameMapLength = 8;
const GameMapWidth = 8;

module.exports = {
  Direction,
  GameMapLength,
  GameMapWidth,
};

},{}],9:[function(require,module,exports){
const { GameMapWidth, GameMapLength } = require('./GameEnum');
const GameData = require('./GameData');
const Position = require('./Component/Position');
const Snake = require('./Component/Snake');
const Apple = require('./Component/Apple');
const { getRandomInteger } = require('./utils/MathUtility');

function isDirectionValid(direction, snakeBodyHold) {
  return direction !== snakeBodyHold;
}

function print(gameMap) {
  const e = new CustomEvent('UpdateGameMapOnDOM', {
    detail: {
      gameMap,
    },
  });
  window.dispatchEvent(e);

  let str = '';
  gameMap.forEach((subArr) => {
    str += `${subArr.join(' ')}\n`;
  });
  console.log(str);
}

function getValidPos(currOcupiedPos) {
  let isInvalid = true;
  let randRow = null;
  let randCol = null;

  do {
    randRow = getRandomInteger(0, GameMapWidth - 1);
    randCol = getRandomInteger(0, GameMapLength - 1);
    isInvalid = currOcupiedPos.some(pos => pos.row === randRow && pos.col === randCol);
  } while (isInvalid);

  return { row: randRow, col: randCol };
}

class GameLogic {
  constructor() {
    this.gameMap = [];
    this.snake = new Snake();
    this.apples = [];
    this._initGameMap();
  }

  reset() {
    for (let i = 0; i < GameMapWidth; i += 1) {
      for (let j = 0; j < GameMapLength; j += 1) {
        this.gameMap[i][j] = 0;
      }
    }

    this.snake = new Snake();
    this.apples.length = 0;

    this._addSnakeOnGameMap();
  }

  _initGameMap() {
    this.gameMap = new Array(GameMapWidth);
    for (let i = 0; i < GameMapWidth; i += 1) {
      this.gameMap[i] = new Array(GameMapLength);
      for (let j = 0; j < GameMapLength; j += 1) {
        this.gameMap[i][j] = 0;
      }
    }
  }

  _checkEatApple(snakeHeadPos) {
    let appleEaten = null;
    this.apples.some((apple) => {
      if (snakeHeadPos.equal(apple.pos)) {
        appleEaten = apple;
        return true;
      }
      return false;
    });
    return appleEaten;
  }

  _addSnakeOnGameMap() {
    const { prevBody: prevSnake, body: newSnake } = this.snake.getBody();

    this._removePrevSnakeOnGameMap(prevSnake);

    const appleEaten = this._checkEatApple(newSnake[0]);
    if (appleEaten !== null) {
      this._removeAppleInDataArray(appleEaten);
      this._removeAppleOnGameMap(appleEaten);

      if (!GameData.isCorrectAppleEaten(appleEaten)) {
        // 重新產生一個被吃掉的 apple 到場景上
        this.reassignApple(appleEaten);

        // problem: 如果用 event 傳去 index 再更新 GameData, 可能會來不及更新 isEnd 狀態
        GameData.updateHealthPoints(-1);
        window.dispatchEvent(new CustomEvent('UpdateHealthPoints'));

        if (GameData.isEnd) {
          window.dispatchEvent(new CustomEvent('EndOfGame'));
        }
      } else {
        const eventEatApple = new CustomEvent('SnakeEatApple', {
          detail: {
            appleEaten,
          },
        });

        window.dispatchEvent(eventEatApple);

        if (GameData.isFinishTask()) {
          window.dispatchEvent(new CustomEvent('UpdateScore'));
          window.dispatchEvent(new CustomEvent('Pause'));
          setTimeout(() => {
            window.dispatchEvent(new CustomEvent('NextRound'));
          }, 500);
        }
      }
    }

    this._addNewSnakeOnGameMap(newSnake);
    print(this.gameMap);
  }

  _removePrevSnakeOnGameMap(prevSnake) {
    prevSnake.forEach((pos) => {
      this.gameMap[pos.row][pos.col] = 0;
    });
  }

  _addNewSnakeOnGameMap(newSnake) {
    newSnake.forEach((pos) => {
      this.gameMap[pos.row][pos.col] = 1;
    });
  }

  _addAppleOnGameMap() {
    this.apples.forEach((apple) => {
      this.gameMap[apple.pos.row][apple.pos.col] = apple.symbol;
    });
  }

  _removeAppleOnGameMap(apple) {
    this.gameMap[apple.pos.row][apple.pos.col] = 0;
  }

  _removeAppleInDataArray(appleEaten) {
    this.apples = this.apples.filter(apple => apple.symbol !== appleEaten.symbol);
  }

  moveSnake() {
    this.snake.move();
    this._addSnakeOnGameMap();
  }

  turnSnakeDirection(direction) {
    // 如果蛇正往右走，不能按 left，在這裡擋掉
    if (!isDirectionValid(direction, this.snake.getNeckDirection())) {
      return;
    }
    this.snake.turnDirection(direction);
  }

  reassignApple(apple) {
    const { body } = this.snake.getBody();
    const currOcupiedPos = [...body.slice(), ...this.apples.map(a => a.pos)];
    const { row, col } = getValidPos(currOcupiedPos);

    this.apples.push(new Apple({ row, col, symbol: apple.symbol }));

    this._addAppleOnGameMap();
    print(this.gameMap);
  }

  assignNewTask() {
    const { body } = this.snake.getBody();
    const currOcupiedPos = body.slice();

    this.apples = GameData.task.map((note) => {
      const { row, col } = getValidPos(currOcupiedPos);
      currOcupiedPos.push(new Position({ row, col }));
      return new Apple({ row, col, symbol: note });
    });

    this._addAppleOnGameMap();
    print(this.gameMap);
  }
}

const gameLogic = new GameLogic();

module.exports = gameLogic;

},{"./Component/Apple":3,"./Component/Position":4,"./Component/Snake":5,"./GameData":7,"./GameEnum":8,"./utils/MathUtility":14}],10:[function(require,module,exports){
const { getNoteByInterval } = require('./Interval');
const { getRandomInteger } = require('../utils/MathUtility');

const ChordType = {
  TriadMajor: 'maj',
  TriadMinor: 'min',
  TriadDiminished: 'dim',

  SeventhMajor: 'maj7',
  SeventhMinor: 'min7',
  SeventhDominant: '7',
  SeventhHalfDiminished: 'm7b5',
  SeventhDiminished: 'dim7',
};

const ChordInfo = {
  [ChordType.TriadMajor]: {
    degrees: ['1', '3', '5'],
    intervals: [0, 4, 7],
  },
  [ChordType.TriadMinor]: {
    degrees: ['1', '3b', '5'],
    intervals: [0, 3, 7],
  },
  [ChordType.TriadDiminished]: {
    degrees: ['1', '3b', '5b'],
    intervals: [0, 3, 6],
  },

  [ChordType.SeventhMajor]: {
    degrees: ['1', '3', '5', '7'],
    intervals: [0, 4, 7, 11],
  },
  [ChordType.SeventhMinor]: {
    degrees: ['1', '3b', '5', '7b'],
    intervals: [0, 3, 7, 10],
  },
  [ChordType.SeventhDominant]: {
    degrees: ['1', '3', '5', '7b'],
    intervals: [0, 4, 7, 10],
  },
  [ChordType.SeventhHalfDiminished]: {
    degrees: ['1', '3b', '5b', '7b'],
    intervals: [0, 3, 6, 10],
  },
  [ChordType.SeventhDiminished]: {
    degrees: ['1', '3b', '5b', '7bb'],
    intervals: [0, 3, 6, 9],
  },
};

/**
 *  @param {String} key    - eg: A, Eb, G#
 *  @param {Number} chordType - eg: maj7, dim7, min,...etc.
 *  #example
 *  getNotesInChord('A', ChordType.Seventh_Major)
 *
 *  return {
 *      notesInChord: ['A', 'C#', 'E', 'G#'],
 *      degrees: ['1', '3', '5', '7']
 *  }
 */
function getNotesInChord(key, chordType) {
  // G maj7
  const { degrees, intervals } = ChordInfo[chordType];

  const notesInChord = [];
  let degree;
  let interval;
  let note;

  for (let i = 0; i < degrees.length; i += 1) {
    degree = parseInt(degrees[i], 10);
    interval = intervals[i];
    note = getNoteByInterval(key, degree, interval);
    notesInChord.push(note);
  }

  return {
    notesInChord,
    degrees,
  };
}

function getRandomChordType() {
  const chordTypes = Object.values(ChordType);
  const randIdx = getRandomInteger(0, chordTypes.length - 1);
  return chordTypes[randIdx];
}

module.exports = {
  ChordType,
  getNotesInChord,
  getRandomChordType,
};

},{"../utils/MathUtility":14,"./Interval":11}],11:[function(require,module,exports){
const KeyAmount = 7;
const IntervalAmount = 12;

const Key = {
  C: 0,
  D: 1,
  E: 2,
  F: 3,
  G: 4,
  A: 5,
  B: 6,
};

const KeyName = {
  [Key.C]: 'C',
  [Key.D]: 'D',
  [Key.E]: 'E',
  [Key.F]: 'F',
  [Key.G]: 'G',
  [Key.A]: 'A',
  [Key.B]: 'B',
};

const KeySignature = {
  None: 0,
  Flat: 1,
  Sharp: 2,
};

const KeySignatureName = {
  [KeySignature.None]: '',
  [KeySignature.Flat]: 'b',
  [KeySignature.Sharp]: '#',
};

// TODO calculated by function
const KeyInterval2DTable = [
  ['C', 'C#', 'C##', 'C###', 'C####', 'C#####', 'Cbbbbbb', 'Cbbbbb', 'Cbbbb', 'Cbbb', 'Cbb', 'Cb'],
  ['Dbb', 'Db', 'D', 'D#', 'D##', 'D###', 'D####', 'D#####', 'Dbbbbbb', 'Dbbbbb', 'Dbbbb', 'Dbbb'],
  ['Ebbbb', 'Ebbb', 'Ebb', 'Eb', 'E', 'E#', 'E##', 'E###', 'E####', 'E#####', 'Ebbbbbb', 'Ebbbbb'],
  ['Fbbbbb', 'Fbbbb', 'Fbbb', 'Fbb', 'Fb', 'F', 'F#', 'F##', 'F###', 'F####', 'F#####', 'Fbbbbbb'],
  ['G#####', 'Gbbbbbb', 'Gbbbbb', 'Gbbbb', 'Gbbb', 'Gbb', 'Gb', 'G', 'G#', 'G##', 'G###', 'G####'],
  ['A###', 'A####', 'A#####', 'Abbbbbb', 'Abbbbb', 'Abbbb', 'Abbb', 'Abb', 'Ab', 'A', 'A#', 'A##'],
  ['B#', 'B##', 'B###', 'B####', 'B#####', 'Bbbbbbb', 'Bbbbbb', 'Bbbbb', 'Bbbb', 'Bbb', 'Bb', 'B'],
];

/**
 *
 *  @param {String} key - eg: G
 *  @return {Object} { keyIdx: 4, intervalIdx: 7}
 */
function findIdxDegreeNInterval(key) {
  const keyIdx = Key[key[0].toUpperCase()];
  const intervalIdx = KeyInterval2DTable[keyIdx].indexOf(key);

  return { keyIdx, intervalIdx };
}

/**
 *  getNoteByInterval(C, 3, 4) // E
 *  @param {String} baseNote - C
 *  @param {Number} degree - 3
 *  @param {Number} intervalDiff - 4
 *  @return {String} - E
 */
function getNoteByInterval(baseNote, degree, intervalDiff) {
  const { keyIdx: baseKeyIdx, intervalIdx: baseIntervalIdx } = findIdxDegreeNInterval(baseNote);

  const outputKeyIdx = (baseKeyIdx + degree - 1) % KeyAmount;
  const outputIntervalIdx = (baseIntervalIdx + intervalDiff) % IntervalAmount;

  const outputKey = KeyInterval2DTable[outputKeyIdx][outputIntervalIdx];

  return outputKey;
}

module.exports = {
  getNoteByInterval,
  KeyName,
  KeySignatureName,
};

},{}],12:[function(require,module,exports){
const { getNotesInChord, getRandomChordType } = require('./Chord');
const { KeyName, KeySignatureName } = require('./Interval');
const { getRandomInteger } = require('../utils/MathUtility');

/**
 *  @return {Object} - {baseKey, signature, chordType}
 */
function getRandomQuestion() {
  // 七個白鍵
  const baseKeyIdx = getRandomInteger(0, 6);
  // 只考慮 還原、b、# 三種升降記號
  const signatureIdx = getRandomInteger(0, 2);
  // 根據使用者選擇要哪些和弦種類隨機取一個
  const chordType = getRandomChordType();

  return {
    baseKey: KeyName[baseKeyIdx],
    signature: KeySignatureName[signatureIdx],
    chordType,
  };
}

function assignNewTask() {
  const { baseKey, signature, chordType } = getRandomQuestion();
  const key = baseKey + signature;
  const { notesInChord, degrees } = getNotesInChord(key, chordType);
  const chordName = [baseKey, signature, chordType];

  return { notesInChord, degrees, chordName };
}

module.exports = {
  assignNewTask,
};

},{"../utils/MathUtility":14,"./Chord":10,"./Interval":11}],13:[function(require,module,exports){
function createElementWithClass(parent, className) {
  const node = document.createElement('div');
  parent.appendChild(node);
  node.classList.add(className);
  return node;
}

function drawDOM(gameMap) {
  const gameMapEle = document.querySelector('#game-map');

  const rowLength = gameMap.length;
  const colLength = gameMap[0].length;

  if (gameMapEle.children.length === 0) {
    for (let i = 0; i < rowLength; i += 1) {
      for (let j = 0; j < colLength; j += 1) {
        const node = createElementWithClass(gameMapEle, 'block');
        const gameMapData = gameMap[i][j];
        const isPath = gameMapData === 0;
        const isSnake = gameMapData === 1;
        const isApple = !isPath && !isSnake;
        const text = !isApple ? '' : gameMapData;

        node.innerHTML = text;

        if (!isPath) {
          const className = isSnake ? 'snake' : 'apple';
          node.classList.add(className);
        }
      }
    }
  } else {
    for (let i = 0; i < rowLength; i += 1) {
      for (let j = 0; j < colLength; j += 1) {
        const node = gameMapEle.children[i * colLength + j];
        node.className = 'block';
        const gameMapData = gameMap[i][j];
        const isPath = gameMapData === 0;
        const isSnake = gameMapData === 1;
        const isApple = !isPath && !isSnake;
        const text = !isApple ? '' : gameMapData;

        node.innerHTML = text;

        if (!isPath) {
          const className = isSnake ? 'snake' : 'apple';
          node.classList.add(className);
        }
      }
    }
  }
}

module.exports = {
  createElementWithClass,
  drawDOM,
};

},{}],14:[function(require,module,exports){
function getRandomInteger(from, to) {
  const length = Math.abs(to - from) + 1;
  return Math.floor(Math.random() * length) + from;
}

module.exports = {
  getRandomInteger,
};

},{}]},{},[1]);
