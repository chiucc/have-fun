const { Direction } = require('./script/GameEnum');
const GameData = require('./script/GameData');
const GameLogic = require('./script/GameLogic');
const Console = require('./script/Console');
const NewTaskHelper = require('./script/MusicTheory/NewTaskHelper');
const AnimationControl = require('./script/AnimationControl');
const { drawDOM } = require('./script/utils/DOMUtility');

function makeSureRequestAnimationFrame() {
  const requestAnimationFrame = window.requestAnimationFrame
    || window.mozRequestAnimationFrame
    || window.webkitRequestAnimationFrame
    || window.msRequestAnimationFrame;
  window.requestAnimationFrame = requestAnimationFrame;
}

function assignNewTask() {
  const { notesInChord: notes, degrees, chordName } = NewTaskHelper.assignNewTask();

  GameData.assignNewTask(notes, degrees, chordName);
  Console.assignNewTask();
  GameLogic.assignNewTask();

  GameData.updatePauseStatus(false);
  AnimationControl.setStop(false);
  AnimationControl.resume();
}

function startGame() {
  GameLogic.reset();
  GameData.reset();
  AnimationControl.setStop(false);

  const { fps } = GameData;
  const initialCallback = assignNewTask;
  const everyFrameCallbcak = GameLogic.moveSnake.bind(GameLogic);

  AnimationControl.setEveryFrameCallbcak(everyFrameCallbcak);
  AnimationControl.startAnimating(fps, initialCallback);

  Console.updateBtnWording();
  Console.closeGameMap(false);
}

function togglePause() {
  const togglePauseStatus = !GameData.isPause;
  GameData.updatePauseStatus(togglePauseStatus);
  Console.updatePauseBtnText();

  AnimationControl.setStop(togglePauseStatus);
  if (!togglePauseStatus) {
    AnimationControl.resume();
  }
}

function addListener() {
  window.addEventListener('NextRound', () => {
    assignNewTask();
  });

  window.addEventListener('Pause', () => {
    GameData.updatePauseStatus(true);
    AnimationControl.setStop(true);
    AnimationControl.resume();
  });

  window.addEventListener('SnakeEatApple', (e) => {
    const { appleEaten } = e.detail;
    GameData.updateCurrApple(appleEaten);
    Console.updateInProgress();
  });

  window.addEventListener('UpdateScore', () => {
    const gainScore = 1;
    GameData.updateScore(gainScore);
  });

  window.addEventListener('UpdateHealthPoints', () => {
    Console.updateHealthPoints();
  });

  window.addEventListener('UpdateGameMapOnDOM', (e) => {
    const { gameMap } = e.detail;
    drawDOM(gameMap);
  });

  window.addEventListener('EndOfGame', () => {
    console.log('End Of Game!');

    GameData.updateEndStatus(true);
    AnimationControl.setStop(true);
    Console.updateBtnWording();
    Console.closeGameMap(true);
  });

  document.addEventListener('keydown', (e) => {
    const { keyCode } = e;
    e.preventDefault();

    if (GameData.isPause || GameData.isEnd) {
      return;
    }

    switch (keyCode) {
      case 37: // left arrow
        GameLogic.turnSnakeDirection(Direction.Left);
        break;
      case 39: // right arrow
        GameLogic.turnSnakeDirection(Direction.Right);
        break;
      case 40: // down arrow
        GameLogic.turnSnakeDirection(Direction.Down);
        break;
      case 38: // up arrow
        GameLogic.turnSnakeDirection(Direction.Up);
        break;
      default:
        break;
    }
  });

  document.querySelector('#start-btn').addEventListener('click', () => {
    if (GameData.isEnd) {
      startGame();
    }
  });

  document.querySelector('#pause-btn').addEventListener('click', () => {
    if (!GameData.isEnd) {
      togglePause();
    }
  });
}

makeSureRequestAnimationFrame();
addListener();
Console.updateBtnWording();
