(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
(function() {
  const GameLogic = require("./script/GameLogic");
  const AnimationControl = require("./script/AnimationControl");
  const DOMUtility = require("./script/utils/DOMUtility");
  const GameEnum = require("./script/GameEnum");
  const MoveDirection = GameEnum.MoveDirection;
  const RotateDirection = GameEnum.RotateDirection;

  const gameLogic = new GameLogic();
  const animationControl = new AnimationControl();

  function startGame() {
    gameLogic.reset();
    gameLogic.setEndOfGame(false);
    animationControl.setStop(false);

    const fps = gameLogic.getFramePerSecond();
    const initialCallback = gameLogic.assignNewBlock.bind(gameLogic);
    const everyFrameCallbcak = gameLogic.moveBlock.bind(
      gameLogic,
      MoveDirection.Down
    );

    animationControl.setEveryFrameCallbcak(everyFrameCallbcak);
    animationControl.startAnimating(fps, initialCallback);
  }

  function showDialog(toShow) {
    let modal = document.querySelector("#myModal");
    modal.style.display = toShow ? "block" : "none";
  }

  function makeSureRequestAnimationFrame() {
    let requestAnimationFrame =
      window.requestAnimationFrame ||
      window.mozRequestAnimationFrame ||
      window.webkitRequestAnimationFrame ||
      window.msRequestAnimationFrame;
    window.requestAnimationFrame = requestAnimationFrame;
  }

  function addListener() {
    window.addEventListener("NextRound", () => {
      gameLogic.assignNewBlock();
    });

    window.addEventListener("MoveRecover", () => {
      gameLogic.recoverBlockPos();
    });

    window.addEventListener("RotateRecover", () => {
      gameLogic.recoverBlockShape();
    });

    window.addEventListener("ShiftPosCol", e => {
      const posColDiff = e.detail.posColDiff;
      gameLogic.updateBlockPosByColDiff(posColDiff);
    });

    window.addEventListener("EndOfGame", () => {
      console.log("End Of Game!");
      showDialog(true);
      gameLogic.setEndOfGame(true);
      animationControl.setStop(true);
    });

    window.addEventListener("Draw", e => {
      const bucketData = e.detail.bucket;
      DOMUtility.updateBucket(bucketData);
    });

    document.addEventListener("keydown", e => {
      const keyCode = e.keyCode;
      event.preventDefault();

      if (gameLogic.isEndOfGame()) {
        return;
      }

      switch (keyCode) {
        case 37: // left arrow
          gameLogic.moveBlock(MoveDirection.Left);
          break;
        case 39: // right arrow
          gameLogic.moveBlock(MoveDirection.Right);
          break;
        case 40: // down arrow
          gameLogic.moveBlock(MoveDirection.Down);
          break;
        case 38: // up arrow
          gameLogic.rotateBlock(RotateDirection.Clockwise);
          break;
        case 32: // space, to bottom
          gameLogic.moveBlockToBottom();
        default:
          break;
      }
    });

    document.querySelector("#startGameBtn").addEventListener("click", () => {
      showDialog(false);
      startGame();
    });
  }

  makeSureRequestAnimationFrame();
  addListener();
})();

},{"./script/AnimationControl":2,"./script/GameEnum":7,"./script/GameLogic":8,"./script/utils/DOMUtility":11}],2:[function(require,module,exports){
/*
    for animation
    ref: https://stackoverflow.com/a/19772220
         http://jsfiddle.net/m1erickson/CtsY3/
*/
class AnaimationControl {
  constructor() {
    this.fps = null;
    this.fpsInterval = null;
    this.startTime = null;
    this.now = null;
    this.then = null;
    this.elapsed = null;

    this.isStop = false;

    this.everyFrameCallback = null;
  }

  setStop(isStop) {
    this.isStop = isStop;
  }

  setEveryFrameCallbcak(everyFrameCallback) {
    this.everyFrameCallback = everyFrameCallback;
  }

  startAnimating(fps = 30, initialCallback) {
    this.fps = fps;
    this.fpsInterval = 1000 / this.fps;
    this.then = Date.now();
    this.startTime = this.then;

    console.log(` startAnimating, startTime: ${this.startTime}`);

    // GameLogic.assignNewBlock();
    initialCallback && initialCallback();
    this.animate();
  }

  animate() {
    if (this.isStop) {
      return;
    }

    // request another frame
    window.requestAnimationFrame(this.animate.bind(this));

    // calculate elapsed time since last loop
    this.now = Date.now();
    this.elapsed = this.now - this.then;

    // if enough time has elapsed, draw the next frame
    if (this.elapsed > this.fpsInterval) {
      // Get ready for next frame by setting then=now, but...
      // Also, adjust for fpsInterval not being multiple of 16.67
      this.then = this.now - (this.elapsed % this.fpsInterval);

      // draw stuff here
      // GameLogic.moveBlock(MoveDirection.Down);
      this.everyFrameCallback && this.everyFrameCallback();
    }
  }
}

module.exports = AnaimationControl;

},{}],3:[function(require,module,exports){
const Position = require("./Position");
const ArrayUtility = require("../utils/ArrayUtility");
const MathUtility = require("../utils/MathUtility");
const GameEnum = require("../GameEnum");
const RotateDirection = GameEnum.RotateDirection;

// integer 不能動, 在 getRandomBlock() 有用到
const Type = {
  Stone: 1,
  L: 2,
  LReverse: 3,
  Z: 4,
  ZReverse: 5,
  T: 6,
  Line: 7,
  LineReverse: 8,

  None: -1
};

const Shape = {
  [Type.Line]: [[1, 1, 1, 1]],

  [Type.Stone]: [[1, 1], [1, 1]],

  [Type.L]: [[0, 0, 1], [1, 1, 1]],

  [Type.LReverse]: [[1, 0, 0], [1, 1, 1]],

  [Type.Z]: [[1, 1, 0], [0, 1, 1]],

  [Type.ZReverse]: [[0, 1, 1], [1, 1, 0]],

  [Type.T]: [[0, 1, 0], [1, 1, 1]],

  [Type.LineReverse]: [[1], [1], [1], [1]],

  [Type.None]: [[]]
};

function getRandomBlock() {
  // LineReverse, None 不進去
  return MathUtility.getRandomInteger(Type.Stone, Type.Line);
}

class Block {
  constructor(type, pos) {
    this.type = type;

    this.prevShape = Shape[Type.None];
    this.currShape = Shape[type];

    this.prevPos = new Position();
    this.currPos = new Position({ row: pos.row, col: pos.col });

    /**
     *  anchor 是一個作為旋轉中心的假想機制
     *  pos 是原本的位置，扣掉 anchor 後就會把 anchor 那個點當作中心
     */
    this.currAnchor = new Position();
    this.prevAnchor = new Position();

    this._setAnchorPos();
  }

  _setAnchorPos() {
    if (ArrayUtility.deepEqual2DArr(this.prevShape, Shape[Type.None])) {
      this.prevAnchor.reset();
    } else {
      this.prevAnchor.setPos({
        row: this.currAnchor.row,
        col: this.currAnchor.col
      });
    }

    if (ArrayUtility.deepEqual2DArr(this.currShape, Shape[Type.Line])) {
      this.currAnchor.setPos({ row: 0, col: 1 });
    } else if (
      ArrayUtility.deepEqual2DArr(this.currShape, Shape[Type.LineReverse])
    ) {
      this.currAnchor.setPos({ row: 1, col: 0 });
    } else {
      this.currAnchor.setPos({ row: 0, col: 0 });
    }
  }

  updateShapeByRotate(direction) {
    this.prevShape = this.currShape;

    switch (direction) {
      case RotateDirection.Clockwise:
        this.currShape = ArrayUtility.rotateArrayClockwise(this.prevShape);
        break;
      case RotateDirection.Counterclockwise:
        this.currShape = ArrayUtility.rotateArrayCounterclockwise(
          this.prevShape
        );
        break;
      default:
        console.error("wrong rotate direction!");
        return;
    }

    this._setAnchorPos();
  }

  updatePosByDelta({ rowDelta, colDelta }) {
    this.prevPos.setPos({
      row: this.currPos.row,
      col: this.currPos.col
    });

    this.currPos.updatePosByDelta({ rowDelta, colDelta });

    this.prevShape = this.currShape;

    this._setAnchorPos();
  }

  updatePosByColDiff(posColDiff) {
    this.prevPos.setPos({
      row: this.prevPos.row,
      col: this.prevPos.col - posColDiff
    });

    this.currPos.setPos({
      row: this.currPos.row,
      col: this.currPos.col - posColDiff
    });
  }

  recoverShape() {
    this.currShape = this.prevShape;
    this.prevShape = Shape[Type.None];

    this._setAnchorPos();
  }

  recoverPos() {
    this.currPos.setPos({
      row: this.prevPos.row,
      col: this.prevPos.col
    });

    this.prevPos.reset();
  }
}

module.exports = {
  Block,
  getRandomBlock
};

},{"../GameEnum":7,"../utils/ArrayUtility":9,"../utils/MathUtility":12,"./Position":5}],4:[function(require,module,exports){
const Position = require("./Position");
const ArrayUtility = require("../utils/ArrayUtility");
const BucketUtility = require("../utils/BucketUtility");

const BucketWidth = 10;
const BucketHeight = 16;

class Bucket {
  constructor(colWidth = BucketWidth, rowHeight = BucketHeight) {
    this.colWidth = colWidth;
    this.rowHeight = rowHeight;

    // 2維陣列, 表示 block 可以移動的地圖
    this.bucket = [];
    // 記錄每個 row 是否已滿, 滿了要消方塊
    this.rowCount = [];

    this.reset();
  }

  reset() {
    if (this.bucket.length !== 0) {
      for (let row = 0; row < this.rowHeight; row++) {
        for (let col = 0; col < this.colWidth; col++) {
          this.bucket[row][col] = 0;
        }
      }
    } else {
      for (let row = 0; row < this.rowHeight; row++) {
        this.bucket[row] = [];
        for (let col = 0; col < this.colWidth; col++) {
          this.bucket[row][col] = 0;
        }
      }
    }

    if (this.rowCount.length === 0) {
      this.rowCount = new Array(this.rowHeight);
    }
    this.rowCount.fill(0);
  }

  updateBucketByMove(block) {
    const prevPos = block.prevPos;
    const currPos = block.currPos;
    const prevAnchor = block.prevAnchor;
    const currAnchor = block.currAnchor;
    const prevShape = block.prevShape;
    const currShape = block.currShape;
    const type = block.type;

    const prevPosArr = this._convertToBucketSpace(
      prevShape,
      prevPos,
      prevAnchor
    );
    const currPosArr = this._convertToBucketSpace(
      currShape,
      currPos,
      currAnchor
    );

    // 先把舊的 block 拿掉, 之後檢察不通過的話再加回來
    this._deleteBlockInBucket(prevPosArr);

    // 檢查 currPos 會不會碰到現有的 block, 不會的話回傳 nextRound, 送新的 block 進來
    const isOverTop = BucketUtility.checkOverTop(currPosArr);
    const isTouchWall = BucketUtility.checkTouchWall(currPosArr, this.colWidth);
    const isTouchBottom = BucketUtility.checkTouchBottom(
      currPosArr,
      this.rowHeight
    );
    const isOutsideBucket = isTouchWall || isOverTop || isTouchBottom;
    const isHorizontalTouchOther = BucketUtility.checkHorizontalTouchOther(
      this.bucket,
      isOutsideBucket,
      currPosArr,
      prevPosArr
    );
    const isVerticalTouchOther = BucketUtility.checkVerticalTouchOther(
      this.bucket,
      isOutsideBucket,
      currPosArr,
      prevPosArr
    );
    const isTouchOtherBlock = BucketUtility.checkTouchOtherBlock(
      this.bucket,
      isOutsideBucket,
      currPosArr
    );
    const isEndOfGame = BucketUtility.checkEndOfGame(
      currPosArr,
      isTouchOtherBlock
    );

    if (isEndOfGame) {
      this._addBlockInBucket(currPosArr, type);
      const event = new CustomEvent("EndOfGame");
      window.dispatchEvent(event);
      return false;
    }

    if (isTouchWall || isHorizontalTouchOther) {
      this._addBlockInBucket(prevPosArr, type);
      const event = new CustomEvent("MoveRecover");
      window.dispatchEvent(event);
      return false;
    }

    if (isTouchBottom || isVerticalTouchOther) {
      this._addBlockInBucket(prevPosArr, type);
      let fullRowObjArr = BucketUtility.checkEliminateRow(
        this.rowCount,
        this.colWidth
      );

      if (fullRowObjArr.length) {
        // 這裡加 promise 演動畫, 再傳 NextRound

        fullRowObjArr.forEach(obj => {
          this.bucket = ArrayUtility.shiftByRows(
            this.bucket,
            obj.rowIdx,
            obj.amount
          );
        });

        this._updateRowCountByBucket();

        print(this.bucket);
      }

      const event = new CustomEvent("NextRound");
      window.dispatchEvent(event);

      this._drawOnDOM();
      return false;
    } else {
      this._addBlockInBucket(currPosArr, type);
      this._updateRowCount(currPosArr, prevPosArr);

      print(this.bucket);
      this._drawOnDOM();
      return true;
    }
  }

  updateBucketByRotate(block) {
    const currPos = block.currPos;
    const prevAnchor = block.prevAnchor;
    const currAnchor = block.currAnchor;
    const prevShape = block.prevShape;
    const currShape = block.currShape;
    const type = block.type;

    const prevPosArr = this._convertToBucketSpace(
      prevShape,
      currPos,
      prevAnchor
    );
    const currPosArr = this._convertToBucketSpace(
      currShape,
      currPos,
      currAnchor
    );

    // 先把舊的 block 拿掉, 之後檢察不通過的話再加回來
    this._deleteBlockInBucket(prevPosArr);

    // 檢查 currPos 會不會碰到現有的 block, 不會的話回傳 nextRound, 送新的 block 進來
    const isTouchWall = BucketUtility.checkTouchWall(currPosArr, this.colWidth);
    const isOverTop = BucketUtility.checkOverTop(currPosArr);
    const isTouchBottom = BucketUtility.checkTouchBottom(
      currPosArr,
      this.rowHeight
    );
    const isOutsideBucket = isTouchWall || isOverTop || isTouchBottom;
    const isTouchOtherBlock = BucketUtility.checkTouchOtherBlock(
      this.bucket,
      isOutsideBucket,
      currPosArr
    );

    if (isOverTop || isTouchBottom || isTouchOtherBlock) {
      this._addBlockInBucket(prevPosArr, type);
      const event = new CustomEvent("RotateRecover");
      window.dispatchEvent(event);
    } else {
      // 這裡要修正 pos, 讓任何時候都可以旋轉
      // 計算超過幾個 col, 扣回來
      const posColDiff = isTouchWall
        ? BucketUtility.getColOverAmount(currPosArr, BucketWidth)
        : 0;
      const modifiedCurrPosArr = BucketUtility.updateCurrPosByColDiff(
        currPosArr,
        posColDiff
      );

      this._addBlockInBucket(modifiedCurrPosArr, type);
      this._updateRowCount(modifiedCurrPosArr, prevPosArr);

      if (isTouchWall && posColDiff !== 0) {
        const event = new CustomEvent("ShiftPosCol", {
          detail: {
            posColDiff
          }
        });
        window.dispatchEvent(event);
      }

      print(this.bucket);
    }

    this._drawOnDOM();
  }

  _deleteBlockInBucket(blockInBucketSpace) {
    blockInBucketSpace.forEach(pos => {
      this.bucket[pos.row][pos.col] = 0;
    });
  }

  _addBlockInBucket(blockInBucketSpace, type) {
    blockInBucketSpace.forEach(pos => (this.bucket[pos.row][pos.col] = type));
  }

  _updateRowCount(currPosArr, prevPosArr) {
    prevPosArr.forEach(pos => this.rowCount[pos.row]--);
    currPosArr.forEach(pos => this.rowCount[pos.row]++);
  }

  _updateRowCountByBucket() {
    // 很暴力, 待優化

    this.bucket.forEach((rowArr, idx) => {
      this.rowCount[idx] = rowArr.reduce((prev, curr) => {
        return curr !== 0 ? prev + 1 : prev;
      }, 0);
    });
  }

  /**
   *
   *  @param {Array[Array]} blockShape
   *  @param {Position} refPos - blockShape 在 bucket 的位置
   *  @param {Position} anchorPos - blockShape 中作為轉軸點的位置
   *
   *  @return {Array[Position]} mapArray - blockShape 在 bucket 的座標
   */
  _convertToBucketSpace(blockShape, refPos, anchorPos) {
    /**
     *  blockShape = [[1,1,1]
     *                [0,1,0]]
     *
     *  (posRow, posCol) = (1,2)
     *
     *  map = (0,0) -> (1,2)
     *        (0,1) -> (1,3)
     *        (0,2) -> (1,4)
     *        (1,1) -> (2,3)
     */
    if (refPos.row === -1 && refPos.col === -1) {
      return [];
    }

    let mapArray = [];
    blockShape.forEach((rowArr, row) => {
      rowArr.forEach((ele, col) => {
        if (ele !== 0) {
          mapArray.push(
            new Position({
              row: row + refPos.row - anchorPos.row,
              col: col + refPos.col - anchorPos.col
            })
          );
        }
      });
    });

    return mapArray;
  }

  // 這可以優化成, 與 updateBucket 一樣, 只要更新 currPosArr, prevPosArr 裡面的座標就好
  _drawOnDOM() {
    const event = new CustomEvent("Draw", {
      detail: {
        bucket: this.bucket.slice()
      }
    });
    window.dispatchEvent(event);
  }
}

function print(arr) {
  let str = "";
  arr.forEach(ele => (str += ele.join(" ") + "\n"));
  console.log(str);
}

module.exports = {
  Bucket,
  BucketWidth,
  BucketHeight
};

},{"../utils/ArrayUtility":9,"../utils/BucketUtility":10,"./Position":5}],5:[function(require,module,exports){
class Position {
  constructor({ row = -1, col = -1 } = {}) {
    this.setPos({ row, col });
  }

  setPos({ row, col }) {
    this.row = row;
    this.col = col;
  }

  updatePosByDelta({ rowDelta, colDelta }) {
    this.row += rowDelta;
    this.col += colDelta;
  }

  reset() {
    this.setPos({ row: -1, col: -1 });
  }
}

module.exports = Position;

},{}],6:[function(require,module,exports){
class GameData {
  constructor() {
    this.reset();
  }

  reset() {
    this.fps = 1;
    this.isEndOfGame = true;
  }
}

module.exports = GameData;

},{}],7:[function(require,module,exports){
const MoveDirection = {
  Down: 0,
  Left: 1,
  Right: 2
};

const RotateDirection = {
  Clockwise: 0,
  Counterclockwise: 1
};

const GameState = {
  NewGame: 0,
  EndOfGame: 1
};

module.exports = {
  MoveDirection,
  RotateDirection
};

},{}],8:[function(require,module,exports){
const GameData = require("./GameData");
const Bucket = require("./Component/Bucket");
const Block = require("./Component/Block");

const GameEnum = require("./GameEnum");
const MoveDirection = GameEnum.MoveDirection;

class GameLogic {
  constructor() {
    // ref: https://docs.gitlab.com/ce/development/fe_guide/design_patterns.html#singletons
    if (!GameLogic.prototype._singletonInstance) {
      this._init();
      GameLogic.prototype._singletonInstance = this;
    }

    return GameLogic.prototype._singletonInstance;
  }

  _init() {
    this.gameData = new GameData();
    this.bucket = new Bucket.Bucket();
    this.block = null;
  }

  reset() {
    this.gameData.reset();
    this.bucket.reset();
    this.block = null;
  }

  assignNewBlock() {
    const randBlockType = Block.getRandomBlock();
    const initialPos = {
      // top of vertical direction
      row: 0,
      // center of horizontal direction
      col: Math.floor(Bucket.BucketWidth / 2) - 1
    };

    this.block = new Block.Block(randBlockType, initialPos);
    this.bucket.updateBucketByMove(this.block);
  }

  moveBlockToBottom() {
    let notTouchBottom = false;
    do {
      notTouchBottom = this.moveBlock(MoveDirection.Down);
    } while (notTouchBottom);
  }

  moveBlock(direction) {
    let rowDelta = 0;
    let colDelta = 0;
    switch (direction) {
      case MoveDirection.Down:
        rowDelta = 1;
        colDelta = 0;
        break;
      case MoveDirection.Right:
        rowDelta = 0;
        colDelta = 1;
        break;
      case MoveDirection.Left:
        rowDelta = 0;
        colDelta = -1;
        break;
      default:
        console.error("wrong move direction!");
        return;
    }

    this.block.updatePosByDelta({ rowDelta, colDelta });
    return this.bucket.updateBucketByMove(this.block);
  }

  rotateBlock(direction) {
    this.block.updateShapeByRotate(direction);
    this.bucket.updateBucketByRotate(this.block);
  }

  updateBlockPosByColDiff(posColDiff) {
    this.block.updatePosByColDiff(posColDiff);
  }

  recoverBlockPos() {
    this.block.recoverPos();
  }

  recoverBlockShape() {
    this.block.recoverShape();
  }

  isEndOfGame() {
    return this.gameData.isEndOfGame;
  }

  setEndOfGame(state) {
    this.gameData.isEndOfGame = state;
  }

  getFramePerSecond() {
    return this.gameData.fps;
  }
}

module.exports = GameLogic;

},{"./Component/Block":3,"./Component/Bucket":4,"./GameData":6,"./GameEnum":7}],9:[function(require,module,exports){
/**
 *
 *  @param {Array[Array]} array
 *  @return {Array[Array]}
 *
 *  @example
 *  turn [[1 0 0]    to  [[1 1]
 *        [1 1 1]]        [1 0]
 *                        [1 0]]
 */
function rotateArrayClockwise(array) {
  if (array.length === 0) {
    return [[]];
  }

  let originalNumOfRow = array.length;
  let originalNumOfCol = array[0].length;

  let newArray = new Array(originalNumOfCol);

  for (let i = 0; i < originalNumOfCol; i++) {
    newArray[i] = new Array(originalNumOfRow);

    for (let j = 0; j < originalNumOfRow; j++) {
      newArray[i][j] = array[originalNumOfRow - j - 1][i];
    }
  }
  return newArray;
}

/**
 *
 *  @param {Array[Array]} array
 *  @return {Array[Array]}
 *
 *  @example
 *  turn [[1 0 0]   to  [[0 1]
 *        [1 1 1]]       [0 1]
 *                       [1 1]]
 */
function rotateArrayCounterclockwise(array) {
  if (array.length === 0) {
    return [[]];
  }

  let originalNumOfRow = array.length;
  let originalNumOfCol = array[0].length;

  let newArray = new Array(originalNumOfCol);

  for (let i = 0; i < originalNumOfCol; i++) {
    newArray[i] = new Array(originalNumOfRow);

    for (let j = 0; j < originalNumOfRow; j++) {
      newArray[i][j] = array[j][originalNumOfCol - i - 1];
    }
  }
  return newArray;
}

/**
 *  往下平移 amount 個 row
 *  @param {Array[Array]} arr
 *  @param {Number} rowIdx - 要刪除的 row index
 *  @param {Number} amount - 從 rowIdx 開始往下數共幾行
 *
 *  [ 0 0 0 0  shift 1  [ 0 0 0 0
 *    0 1 1 0    =>       0 0 0 0
 *    0 0 1 0             0 1 1 0
 *    0 0 0 0]            0 0 1 0 ]
 */
function shiftByRows(arr, rowIdx, amount) {
  // 要可以一次消多行 row
  let rowLength = arr.length;
  let colLength = arr[0].length;

  // 先清空
  for (let row = rowIdx; row < rowIdx + amount; row++) {
    for (let col = 0; col < colLength; col++) {
      arr[row][col] = 0;
    }
  }

  // 再換過去
  for (let row = rowIdx - 1; row >= 0; row--) {
    for (let col = 0; col < colLength; col++) {
      arr[row + amount][col] = arr[row][col];
      arr[row][col] = 0;
    }
  }

  return arr;
}

function deepEqual2DArr(arr1, arr2) {
  return arr1.every((subarr, i) => {
    return subarr.every((ele, j) => arr2[i] && ele === arr2[i][j]);
  });
}

module.exports = {
  rotateArrayClockwise,
  rotateArrayCounterclockwise,
  shiftByRows,
  deepEqual2DArr
};

},{}],10:[function(require,module,exports){
const Position = require("../Component/Position");

function checkEndOfGame(currPosArr, isTouchOtherBlock) {
  const isTouchTopRow = currPosArr.some(pos => pos.row === 0);

  // 碰到其他 block 又碰到 row 頂端, 就結束遊戲
  return isTouchOtherBlock && isTouchTopRow;
}

function checkTouchOtherBlock(bucket, isOutsideBucket, currPosArr = []) {
  if (isOutsideBucket) {
    return null;
  }

  return currPosArr.some(pos => {
    return bucket[pos.row][pos.col] !== 0;
  });
}

function checkVerticalTouchOther(
  bucket,
  isOutsideBucket,
  currPosArr = [],
  prevPosArr = []
) {
  if (isOutsideBucket || prevPosArr.length === 0) {
    return null;
  }

  return currPosArr.some((pos, i) => {
    // 垂直移動撞到其他方塊
    return pos.col === prevPosArr[i].col && bucket[pos.row][pos.col] !== 0;
  });
}

function checkHorizontalTouchOther(
  bucket,
  isOutsideBucket,
  currPosArr = [],
  prevPosArr = []
) {
  if (isOutsideBucket || prevPosArr.length === 0) {
    return null;
  }

  return currPosArr.some((pos, i) => {
    // 水平移動撞到其他方塊
    return pos.row === prevPosArr[i].row && bucket[pos.row][pos.col] !== 0;
  });
}

function checkTouchWall(currPosArr = [], colWidth) {
  // 檢查 array 裡面的值會不會超過 bucket 的左右範圍
  return currPosArr.some(pos => pos.col < 0 || pos.col >= colWidth);
}

function checkOverTop(currPosArr = []) {
  // 檢查 array 裡面的值會不會超過 bucket 的上界範圍
  return currPosArr.some(pos => pos.row < 0);
}

/**
 *  check if pos in currPosArr exceeds the lower boundary of bucket
 *  @param {Array[Position]} currPosArr
 */
function checkTouchBottom(currPosArr = [], rowHeight) {
  return currPosArr.some(pos => pos.row >= rowHeight);
}

function checkEliminateRow(rowCount = [], colWidth) {
  // check row, 要回傳哪些 row 是 滿的
  const fullRowIdxs = rowCount
    .map((count, idx) => (count === colWidth ? idx : null))
    .filter(idx => idx !== null);

  /* 
    let fullRowIdxs = [3, 5, 6, 7];
    整理成
    objArr = [
        { rowIdx: 3, amount: 1},
        { rowIdx: 5, amount: 3}
    ]
  */
  let rowIdx = -1;
  let amount = 0;
  let objArr = [];
  for (let i = 0; i < fullRowIdxs.length; i++) {
    if (i === 0) {
      rowIdx = fullRowIdxs[i];
      amount++;

      if (i === fullRowIdxs.length - 1) {
        objArr.push({ rowIdx, amount });
      }
    } else {
      if (fullRowIdxs[i] === fullRowIdxs[i - 1] + 1) {
        amount++;

        if (i === fullRowIdxs.length - 1) {
          objArr.push({ rowIdx, amount });
        }
      } else {
        objArr.push({ rowIdx, amount });

        rowIdx = fullRowIdxs[i];
        amount = 1;
      }
    }
  }

  return objArr;
}

function getColOverAmount(currPosArr, bucketWidth) {
  const rightBoundary = bucketWidth - 1;
  const leftBoundary = 0;

  let isLeft = true;
  let pivot = 0;

  currPosArr.forEach(({ col }) => {
    if (col > rightBoundary) {
      isLeft = false;
      if (col > pivot) {
        pivot = col;
      }
    } else if (col < leftBoundary) {
      if (col < pivot) {
        pivot = col;
      }
    }
  });

  const posColDiff = isLeft ? pivot - leftBoundary : pivot - rightBoundary;

  return posColDiff;
}

function updateCurrPosByColDiff(currPosArr, posColDiff) {
  return currPosArr.map(
    pos =>
      new Position({
        row: pos.row,
        col: pos.col - posColDiff
      })
  );
}

module.exports = {
  checkEndOfGame,
  checkTouchOtherBlock,
  checkVerticalTouchOther,
  checkHorizontalTouchOther,
  checkTouchWall,
  checkOverTop,
  checkTouchBottom,
  checkEliminateRow,
  getColOverAmount,
  updateCurrPosByColDiff
};

},{"../Component/Position":5}],11:[function(require,module,exports){
const Bucket = require("../Component/Bucket");
const BucketWidth = Bucket.BucketWidth;
const BucketHeight = Bucket.BucketHeight;

function updateBucket(data) {
  let bucketDiv = document.querySelector("#bucket");
  // generate children, assign row and col

  if (bucketDiv.children.length) {
    for (let row = 0; row < BucketHeight; row++) {
      for (let col = 0; col < BucketWidth; col++) {
        let node = bucketDiv.children[row * BucketWidth + col];
        node.classList.remove(node._type);

        let type = "type" + data[row][col];
        node._type = type;
        node.classList.add(type);
      }
    }
  } else {
    for (let row = 0; row < BucketHeight; row++) {
      for (let col = 0; col < BucketWidth; col++) {
        let node = document.createElement("div");
        bucketDiv.appendChild(node);
        node.classList.add("cell");

        let type = "type" + data[row][col];
        node._type = type;
        node.classList.add(type);
      }
    }
  }
}

module.exports = {
  updateBucket
};

},{"../Component/Bucket":4}],12:[function(require,module,exports){
function getRandomInteger(from, to) {
  const length = Math.abs(to - from) + 1;
  return Math.floor(Math.random() * length) + from;
}

module.exports = {
  getRandomInteger
};

},{}]},{},[1]);
