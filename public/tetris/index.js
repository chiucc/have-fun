(function() {
  const GameLogic = require("./script/GameLogic");
  const AnimationControl = require("./script/AnimationControl");
  const DOMUtility = require("./script/utils/DOMUtility");
  const GameEnum = require("./script/GameEnum");
  const MoveDirection = GameEnum.MoveDirection;
  const RotateDirection = GameEnum.RotateDirection;

  const gameLogic = new GameLogic();
  const animationControl = new AnimationControl();

  function startGame() {
    gameLogic.reset();
    gameLogic.setEndOfGame(false);
    animationControl.setStop(false);

    const fps = gameLogic.getFramePerSecond();
    const initialCallback = gameLogic.assignNewBlock.bind(gameLogic);
    const everyFrameCallbcak = gameLogic.moveBlock.bind(
      gameLogic,
      MoveDirection.Down
    );

    animationControl.setEveryFrameCallbcak(everyFrameCallbcak);
    animationControl.startAnimating(fps, initialCallback);
  }

  function showDialog(toShow) {
    let modal = document.querySelector("#myModal");
    modal.style.display = toShow ? "block" : "none";
  }

  function makeSureRequestAnimationFrame() {
    let requestAnimationFrame =
      window.requestAnimationFrame ||
      window.mozRequestAnimationFrame ||
      window.webkitRequestAnimationFrame ||
      window.msRequestAnimationFrame;
    window.requestAnimationFrame = requestAnimationFrame;
  }

  function addListener() {
    window.addEventListener("NextRound", () => {
      gameLogic.assignNewBlock();
    });

    window.addEventListener("MoveRecover", () => {
      gameLogic.recoverBlockPos();
    });

    window.addEventListener("RotateRecover", () => {
      gameLogic.recoverBlockShape();
    });

    window.addEventListener("ShiftPosCol", e => {
      const posColDiff = e.detail.posColDiff;
      gameLogic.updateBlockPosByColDiff(posColDiff);
    });

    window.addEventListener("EndOfGame", () => {
      console.log("End Of Game!");
      showDialog(true);
      gameLogic.setEndOfGame(true);
      animationControl.setStop(true);
    });

    window.addEventListener("Draw", e => {
      const bucketData = e.detail.bucket;
      DOMUtility.updateBucket(bucketData);
    });

    document.addEventListener("keydown", e => {
      const keyCode = e.keyCode;
      event.preventDefault();

      if (gameLogic.isEndOfGame()) {
        return;
      }

      switch (keyCode) {
        case 37: // left arrow
          gameLogic.moveBlock(MoveDirection.Left);
          break;
        case 39: // right arrow
          gameLogic.moveBlock(MoveDirection.Right);
          break;
        case 40: // down arrow
          gameLogic.moveBlock(MoveDirection.Down);
          break;
        case 38: // up arrow
          gameLogic.rotateBlock(RotateDirection.Clockwise);
          break;
        case 32: // space, to bottom
          gameLogic.moveBlockToBottom();
        default:
          break;
      }
    });

    document.querySelector("#startGameBtn").addEventListener("click", () => {
      showDialog(false);
      startGame();
    });
  }

  makeSureRequestAnimationFrame();
  addListener();
})();
