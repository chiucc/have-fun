/*
    for animation
    ref: https://stackoverflow.com/a/19772220
         http://jsfiddle.net/m1erickson/CtsY3/
*/
class AnaimationControl {
  constructor() {
    this.fps = null;
    this.fpsInterval = null;
    this.startTime = null;
    this.now = null;
    this.then = null;
    this.elapsed = null;

    this.isStop = false;

    this.everyFrameCallback = null;
  }

  setStop(isStop) {
    this.isStop = isStop;
  }

  setEveryFrameCallbcak(everyFrameCallback) {
    this.everyFrameCallback = everyFrameCallback;
  }

  startAnimating(fps = 30, initialCallback) {
    this.fps = fps;
    this.fpsInterval = 1000 / this.fps;
    this.then = Date.now();
    this.startTime = this.then;

    console.log(` startAnimating, startTime: ${this.startTime}`);

    // GameLogic.assignNewBlock();
    initialCallback && initialCallback();
    this.animate();
  }

  animate() {
    if (this.isStop) {
      return;
    }

    // request another frame
    window.requestAnimationFrame(this.animate.bind(this));

    // calculate elapsed time since last loop
    this.now = Date.now();
    this.elapsed = this.now - this.then;

    // if enough time has elapsed, draw the next frame
    if (this.elapsed > this.fpsInterval) {
      // Get ready for next frame by setting then=now, but...
      // Also, adjust for fpsInterval not being multiple of 16.67
      this.then = this.now - (this.elapsed % this.fpsInterval);

      // draw stuff here
      // GameLogic.moveBlock(MoveDirection.Down);
      this.everyFrameCallback && this.everyFrameCallback();
    }
  }
}

module.exports = AnaimationControl;
