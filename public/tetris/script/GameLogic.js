const GameData = require("./GameData");
const Bucket = require("./Component/Bucket");
const Block = require("./Component/Block");

const GameEnum = require("./GameEnum");
const MoveDirection = GameEnum.MoveDirection;

class GameLogic {
  constructor() {
    // ref: https://docs.gitlab.com/ce/development/fe_guide/design_patterns.html#singletons
    if (!GameLogic.prototype._singletonInstance) {
      this._init();
      GameLogic.prototype._singletonInstance = this;
    }

    return GameLogic.prototype._singletonInstance;
  }

  _init() {
    this.gameData = new GameData();
    this.bucket = new Bucket.Bucket();
    this.block = null;
  }

  reset() {
    this.gameData.reset();
    this.bucket.reset();
    this.block = null;
  }

  assignNewBlock() {
    const randBlockType = Block.getRandomBlock();
    const initialPos = {
      // top of vertical direction
      row: 0,
      // center of horizontal direction
      col: Math.floor(Bucket.BucketWidth / 2) - 1
    };

    this.block = new Block.Block(randBlockType, initialPos);
    this.bucket.updateBucketByMove(this.block);
  }

  moveBlockToBottom() {
    let notTouchBottom = false;
    do {
      notTouchBottom = this.moveBlock(MoveDirection.Down);
    } while (notTouchBottom);
  }

  moveBlock(direction) {
    let rowDelta = 0;
    let colDelta = 0;
    switch (direction) {
      case MoveDirection.Down:
        rowDelta = 1;
        colDelta = 0;
        break;
      case MoveDirection.Right:
        rowDelta = 0;
        colDelta = 1;
        break;
      case MoveDirection.Left:
        rowDelta = 0;
        colDelta = -1;
        break;
      default:
        console.error("wrong move direction!");
        return;
    }

    this.block.updatePosByDelta({ rowDelta, colDelta });
    return this.bucket.updateBucketByMove(this.block);
  }

  rotateBlock(direction) {
    this.block.updateShapeByRotate(direction);
    this.bucket.updateBucketByRotate(this.block);
  }

  updateBlockPosByColDiff(posColDiff) {
    this.block.updatePosByColDiff(posColDiff);
  }

  recoverBlockPos() {
    this.block.recoverPos();
  }

  recoverBlockShape() {
    this.block.recoverShape();
  }

  isEndOfGame() {
    return this.gameData.isEndOfGame;
  }

  setEndOfGame(state) {
    this.gameData.isEndOfGame = state;
  }

  getFramePerSecond() {
    return this.gameData.fps;
  }
}

module.exports = GameLogic;
