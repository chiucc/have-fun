class GameData {
  constructor() {
    this.reset();
  }

  reset() {
    this.fps = 1;
    this.isEndOfGame = true;
  }
}

module.exports = GameData;
