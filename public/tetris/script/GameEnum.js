const MoveDirection = {
  Down: 0,
  Left: 1,
  Right: 2
};

const RotateDirection = {
  Clockwise: 0,
  Counterclockwise: 1
};

const GameState = {
  NewGame: 0,
  EndOfGame: 1
};

module.exports = {
  MoveDirection,
  RotateDirection
};
