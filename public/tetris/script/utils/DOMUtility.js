const Bucket = require("../Component/Bucket");
const BucketWidth = Bucket.BucketWidth;
const BucketHeight = Bucket.BucketHeight;

function updateBucket(data) {
  let bucketDiv = document.querySelector("#bucket");
  // generate children, assign row and col

  if (bucketDiv.children.length) {
    for (let row = 0; row < BucketHeight; row++) {
      for (let col = 0; col < BucketWidth; col++) {
        let node = bucketDiv.children[row * BucketWidth + col];
        node.classList.remove(node._type);

        let type = "type" + data[row][col];
        node._type = type;
        node.classList.add(type);
      }
    }
  } else {
    for (let row = 0; row < BucketHeight; row++) {
      for (let col = 0; col < BucketWidth; col++) {
        let node = document.createElement("div");
        bucketDiv.appendChild(node);
        node.classList.add("cell");

        let type = "type" + data[row][col];
        node._type = type;
        node.classList.add(type);
      }
    }
  }
}

module.exports = {
  updateBucket
};
