const Position = require("../Component/Position");

function checkEndOfGame(currPosArr, isTouchOtherBlock) {
  const isTouchTopRow = currPosArr.some(pos => pos.row === 0);

  // 碰到其他 block 又碰到 row 頂端, 就結束遊戲
  return isTouchOtherBlock && isTouchTopRow;
}

function checkTouchOtherBlock(bucket, isOutsideBucket, currPosArr = []) {
  if (isOutsideBucket) {
    return null;
  }

  return currPosArr.some(pos => {
    return bucket[pos.row][pos.col] !== 0;
  });
}

function checkVerticalTouchOther(
  bucket,
  isOutsideBucket,
  currPosArr = [],
  prevPosArr = []
) {
  if (isOutsideBucket || prevPosArr.length === 0) {
    return null;
  }

  return currPosArr.some((pos, i) => {
    // 垂直移動撞到其他方塊
    return pos.col === prevPosArr[i].col && bucket[pos.row][pos.col] !== 0;
  });
}

function checkHorizontalTouchOther(
  bucket,
  isOutsideBucket,
  currPosArr = [],
  prevPosArr = []
) {
  if (isOutsideBucket || prevPosArr.length === 0) {
    return null;
  }

  return currPosArr.some((pos, i) => {
    // 水平移動撞到其他方塊
    return pos.row === prevPosArr[i].row && bucket[pos.row][pos.col] !== 0;
  });
}

function checkTouchWall(currPosArr = [], colWidth) {
  // 檢查 array 裡面的值會不會超過 bucket 的左右範圍
  return currPosArr.some(pos => pos.col < 0 || pos.col >= colWidth);
}

function checkOverTop(currPosArr = []) {
  // 檢查 array 裡面的值會不會超過 bucket 的上界範圍
  return currPosArr.some(pos => pos.row < 0);
}

/**
 *  check if pos in currPosArr exceeds the lower boundary of bucket
 *  @param {Array[Position]} currPosArr
 */
function checkTouchBottom(currPosArr = [], rowHeight) {
  return currPosArr.some(pos => pos.row >= rowHeight);
}

function checkEliminateRow(rowCount = [], colWidth) {
  // check row, 要回傳哪些 row 是 滿的
  const fullRowIdxs = rowCount
    .map((count, idx) => (count === colWidth ? idx : null))
    .filter(idx => idx !== null);

  /* 
    let fullRowIdxs = [3, 5, 6, 7];
    整理成
    objArr = [
        { rowIdx: 3, amount: 1},
        { rowIdx: 5, amount: 3}
    ]
  */
  let rowIdx = -1;
  let amount = 0;
  let objArr = [];
  for (let i = 0; i < fullRowIdxs.length; i++) {
    if (i === 0) {
      rowIdx = fullRowIdxs[i];
      amount++;

      if (i === fullRowIdxs.length - 1) {
        objArr.push({ rowIdx, amount });
      }
    } else {
      if (fullRowIdxs[i] === fullRowIdxs[i - 1] + 1) {
        amount++;

        if (i === fullRowIdxs.length - 1) {
          objArr.push({ rowIdx, amount });
        }
      } else {
        objArr.push({ rowIdx, amount });

        rowIdx = fullRowIdxs[i];
        amount = 1;
      }
    }
  }

  return objArr;
}

function getColOverAmount(currPosArr, bucketWidth) {
  const rightBoundary = bucketWidth - 1;
  const leftBoundary = 0;

  let isLeft = true;
  let pivot = 0;

  currPosArr.forEach(({ col }) => {
    if (col > rightBoundary) {
      isLeft = false;
      if (col > pivot) {
        pivot = col;
      }
    } else if (col < leftBoundary) {
      if (col < pivot) {
        pivot = col;
      }
    }
  });

  const posColDiff = isLeft ? pivot - leftBoundary : pivot - rightBoundary;

  return posColDiff;
}

function updateCurrPosByColDiff(currPosArr, posColDiff) {
  return currPosArr.map(
    pos =>
      new Position({
        row: pos.row,
        col: pos.col - posColDiff
      })
  );
}

module.exports = {
  checkEndOfGame,
  checkTouchOtherBlock,
  checkVerticalTouchOther,
  checkHorizontalTouchOther,
  checkTouchWall,
  checkOverTop,
  checkTouchBottom,
  checkEliminateRow,
  getColOverAmount,
  updateCurrPosByColDiff
};
