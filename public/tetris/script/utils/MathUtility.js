function getRandomInteger(from, to) {
  const length = Math.abs(to - from) + 1;
  return Math.floor(Math.random() * length) + from;
}

module.exports = {
  getRandomInteger
};
