/**
 *
 *  @param {Array[Array]} array
 *  @return {Array[Array]}
 *
 *  @example
 *  turn [[1 0 0]    to  [[1 1]
 *        [1 1 1]]        [1 0]
 *                        [1 0]]
 */
function rotateArrayClockwise(array) {
  if (array.length === 0) {
    return [[]];
  }

  let originalNumOfRow = array.length;
  let originalNumOfCol = array[0].length;

  let newArray = new Array(originalNumOfCol);

  for (let i = 0; i < originalNumOfCol; i++) {
    newArray[i] = new Array(originalNumOfRow);

    for (let j = 0; j < originalNumOfRow; j++) {
      newArray[i][j] = array[originalNumOfRow - j - 1][i];
    }
  }
  return newArray;
}

/**
 *
 *  @param {Array[Array]} array
 *  @return {Array[Array]}
 *
 *  @example
 *  turn [[1 0 0]   to  [[0 1]
 *        [1 1 1]]       [0 1]
 *                       [1 1]]
 */
function rotateArrayCounterclockwise(array) {
  if (array.length === 0) {
    return [[]];
  }

  let originalNumOfRow = array.length;
  let originalNumOfCol = array[0].length;

  let newArray = new Array(originalNumOfCol);

  for (let i = 0; i < originalNumOfCol; i++) {
    newArray[i] = new Array(originalNumOfRow);

    for (let j = 0; j < originalNumOfRow; j++) {
      newArray[i][j] = array[j][originalNumOfCol - i - 1];
    }
  }
  return newArray;
}

/**
 *  往下平移 amount 個 row
 *  @param {Array[Array]} arr
 *  @param {Number} rowIdx - 要刪除的 row index
 *  @param {Number} amount - 從 rowIdx 開始往下數共幾行
 *
 *  [ 0 0 0 0  shift 1  [ 0 0 0 0
 *    0 1 1 0    =>       0 0 0 0
 *    0 0 1 0             0 1 1 0
 *    0 0 0 0]            0 0 1 0 ]
 */
function shiftByRows(arr, rowIdx, amount) {
  // 要可以一次消多行 row
  let rowLength = arr.length;
  let colLength = arr[0].length;

  // 先清空
  for (let row = rowIdx; row < rowIdx + amount; row++) {
    for (let col = 0; col < colLength; col++) {
      arr[row][col] = 0;
    }
  }

  // 再換過去
  for (let row = rowIdx - 1; row >= 0; row--) {
    for (let col = 0; col < colLength; col++) {
      arr[row + amount][col] = arr[row][col];
      arr[row][col] = 0;
    }
  }

  return arr;
}

function deepEqual2DArr(arr1, arr2) {
  return arr1.every((subarr, i) => {
    return subarr.every((ele, j) => arr2[i] && ele === arr2[i][j]);
  });
}

module.exports = {
  rotateArrayClockwise,
  rotateArrayCounterclockwise,
  shiftByRows,
  deepEqual2DArr
};
