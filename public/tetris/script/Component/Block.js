const Position = require("./Position");
const ArrayUtility = require("../utils/ArrayUtility");
const MathUtility = require("../utils/MathUtility");
const GameEnum = require("../GameEnum");
const RotateDirection = GameEnum.RotateDirection;

// integer 不能動, 在 getRandomBlock() 有用到
const Type = {
  Stone: 1,
  L: 2,
  LReverse: 3,
  Z: 4,
  ZReverse: 5,
  T: 6,
  Line: 7,
  LineReverse: 8,

  None: -1
};

const Shape = {
  [Type.Line]: [[1, 1, 1, 1]],

  [Type.Stone]: [[1, 1], [1, 1]],

  [Type.L]: [[0, 0, 1], [1, 1, 1]],

  [Type.LReverse]: [[1, 0, 0], [1, 1, 1]],

  [Type.Z]: [[1, 1, 0], [0, 1, 1]],

  [Type.ZReverse]: [[0, 1, 1], [1, 1, 0]],

  [Type.T]: [[0, 1, 0], [1, 1, 1]],

  [Type.LineReverse]: [[1], [1], [1], [1]],

  [Type.None]: [[]]
};

function getRandomBlock() {
  // LineReverse, None 不進去
  return MathUtility.getRandomInteger(Type.Stone, Type.Line);
}

class Block {
  constructor(type, pos) {
    this.type = type;

    this.prevShape = Shape[Type.None];
    this.currShape = Shape[type];

    this.prevPos = new Position();
    this.currPos = new Position({ row: pos.row, col: pos.col });

    /**
     *  anchor 是一個作為旋轉中心的假想機制
     *  pos 是原本的位置，扣掉 anchor 後就會把 anchor 那個點當作中心
     */
    this.currAnchor = new Position();
    this.prevAnchor = new Position();

    this._setAnchorPos();
  }

  _setAnchorPos() {
    if (ArrayUtility.deepEqual2DArr(this.prevShape, Shape[Type.None])) {
      this.prevAnchor.reset();
    } else {
      this.prevAnchor.setPos({
        row: this.currAnchor.row,
        col: this.currAnchor.col
      });
    }

    if (ArrayUtility.deepEqual2DArr(this.currShape, Shape[Type.Line])) {
      this.currAnchor.setPos({ row: 0, col: 1 });
    } else if (
      ArrayUtility.deepEqual2DArr(this.currShape, Shape[Type.LineReverse])
    ) {
      this.currAnchor.setPos({ row: 1, col: 0 });
    } else {
      this.currAnchor.setPos({ row: 0, col: 0 });
    }
  }

  updateShapeByRotate(direction) {
    this.prevShape = this.currShape;

    switch (direction) {
      case RotateDirection.Clockwise:
        this.currShape = ArrayUtility.rotateArrayClockwise(this.prevShape);
        break;
      case RotateDirection.Counterclockwise:
        this.currShape = ArrayUtility.rotateArrayCounterclockwise(
          this.prevShape
        );
        break;
      default:
        console.error("wrong rotate direction!");
        return;
    }

    this._setAnchorPos();
  }

  updatePosByDelta({ rowDelta, colDelta }) {
    this.prevPos.setPos({
      row: this.currPos.row,
      col: this.currPos.col
    });

    this.currPos.updatePosByDelta({ rowDelta, colDelta });

    this.prevShape = this.currShape;

    this._setAnchorPos();
  }

  updatePosByColDiff(posColDiff) {
    this.prevPos.setPos({
      row: this.prevPos.row,
      col: this.prevPos.col - posColDiff
    });

    this.currPos.setPos({
      row: this.currPos.row,
      col: this.currPos.col - posColDiff
    });
  }

  recoverShape() {
    this.currShape = this.prevShape;
    this.prevShape = Shape[Type.None];

    this._setAnchorPos();
  }

  recoverPos() {
    this.currPos.setPos({
      row: this.prevPos.row,
      col: this.prevPos.col
    });

    this.prevPos.reset();
  }
}

module.exports = {
  Block,
  getRandomBlock
};
