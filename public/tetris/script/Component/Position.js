class Position {
  constructor({ row = -1, col = -1 } = {}) {
    this.setPos({ row, col });
  }

  setPos({ row, col }) {
    this.row = row;
    this.col = col;
  }

  updatePosByDelta({ rowDelta, colDelta }) {
    this.row += rowDelta;
    this.col += colDelta;
  }

  reset() {
    this.setPos({ row: -1, col: -1 });
  }
}

module.exports = Position;
