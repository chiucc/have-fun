const Position = require("./Position");
const ArrayUtility = require("../utils/ArrayUtility");
const BucketUtility = require("../utils/BucketUtility");

const BucketWidth = 10;
const BucketHeight = 16;

class Bucket {
  constructor(colWidth = BucketWidth, rowHeight = BucketHeight) {
    this.colWidth = colWidth;
    this.rowHeight = rowHeight;

    // 2維陣列, 表示 block 可以移動的地圖
    this.bucket = [];
    // 記錄每個 row 是否已滿, 滿了要消方塊
    this.rowCount = [];

    this.reset();
  }

  reset() {
    if (this.bucket.length !== 0) {
      for (let row = 0; row < this.rowHeight; row++) {
        for (let col = 0; col < this.colWidth; col++) {
          this.bucket[row][col] = 0;
        }
      }
    } else {
      for (let row = 0; row < this.rowHeight; row++) {
        this.bucket[row] = [];
        for (let col = 0; col < this.colWidth; col++) {
          this.bucket[row][col] = 0;
        }
      }
    }

    if (this.rowCount.length === 0) {
      this.rowCount = new Array(this.rowHeight);
    }
    this.rowCount.fill(0);
  }

  updateBucketByMove(block) {
    const prevPos = block.prevPos;
    const currPos = block.currPos;
    const prevAnchor = block.prevAnchor;
    const currAnchor = block.currAnchor;
    const prevShape = block.prevShape;
    const currShape = block.currShape;
    const type = block.type;

    const prevPosArr = this._convertToBucketSpace(
      prevShape,
      prevPos,
      prevAnchor
    );
    const currPosArr = this._convertToBucketSpace(
      currShape,
      currPos,
      currAnchor
    );

    // 先把舊的 block 拿掉, 之後檢察不通過的話再加回來
    this._deleteBlockInBucket(prevPosArr);

    // 檢查 currPos 會不會碰到現有的 block, 不會的話回傳 nextRound, 送新的 block 進來
    const isOverTop = BucketUtility.checkOverTop(currPosArr);
    const isTouchWall = BucketUtility.checkTouchWall(currPosArr, this.colWidth);
    const isTouchBottom = BucketUtility.checkTouchBottom(
      currPosArr,
      this.rowHeight
    );
    const isOutsideBucket = isTouchWall || isOverTop || isTouchBottom;
    const isHorizontalTouchOther = BucketUtility.checkHorizontalTouchOther(
      this.bucket,
      isOutsideBucket,
      currPosArr,
      prevPosArr
    );
    const isVerticalTouchOther = BucketUtility.checkVerticalTouchOther(
      this.bucket,
      isOutsideBucket,
      currPosArr,
      prevPosArr
    );
    const isTouchOtherBlock = BucketUtility.checkTouchOtherBlock(
      this.bucket,
      isOutsideBucket,
      currPosArr
    );
    const isEndOfGame = BucketUtility.checkEndOfGame(
      currPosArr,
      isTouchOtherBlock
    );

    if (isEndOfGame) {
      this._addBlockInBucket(currPosArr, type);
      const event = new CustomEvent("EndOfGame");
      window.dispatchEvent(event);
      return false;
    }

    if (isTouchWall || isHorizontalTouchOther) {
      this._addBlockInBucket(prevPosArr, type);
      const event = new CustomEvent("MoveRecover");
      window.dispatchEvent(event);
      return false;
    }

    if (isTouchBottom || isVerticalTouchOther) {
      this._addBlockInBucket(prevPosArr, type);
      let fullRowObjArr = BucketUtility.checkEliminateRow(
        this.rowCount,
        this.colWidth
      );

      if (fullRowObjArr.length) {
        // 這裡加 promise 演動畫, 再傳 NextRound

        fullRowObjArr.forEach(obj => {
          this.bucket = ArrayUtility.shiftByRows(
            this.bucket,
            obj.rowIdx,
            obj.amount
          );
        });

        this._updateRowCountByBucket();

        print(this.bucket);
      }

      const event = new CustomEvent("NextRound");
      window.dispatchEvent(event);

      this._drawOnDOM();
      return false;
    } else {
      this._addBlockInBucket(currPosArr, type);
      this._updateRowCount(currPosArr, prevPosArr);

      print(this.bucket);
      this._drawOnDOM();
      return true;
    }
  }

  updateBucketByRotate(block) {
    const currPos = block.currPos;
    const prevAnchor = block.prevAnchor;
    const currAnchor = block.currAnchor;
    const prevShape = block.prevShape;
    const currShape = block.currShape;
    const type = block.type;

    const prevPosArr = this._convertToBucketSpace(
      prevShape,
      currPos,
      prevAnchor
    );
    const currPosArr = this._convertToBucketSpace(
      currShape,
      currPos,
      currAnchor
    );

    // 先把舊的 block 拿掉, 之後檢察不通過的話再加回來
    this._deleteBlockInBucket(prevPosArr);

    // 檢查 currPos 會不會碰到現有的 block, 不會的話回傳 nextRound, 送新的 block 進來
    const isTouchWall = BucketUtility.checkTouchWall(currPosArr, this.colWidth);
    const isOverTop = BucketUtility.checkOverTop(currPosArr);
    const isTouchBottom = BucketUtility.checkTouchBottom(
      currPosArr,
      this.rowHeight
    );
    const isOutsideBucket = isTouchWall || isOverTop || isTouchBottom;
    const isTouchOtherBlock = BucketUtility.checkTouchOtherBlock(
      this.bucket,
      isOutsideBucket,
      currPosArr
    );

    if (isOverTop || isTouchBottom || isTouchOtherBlock) {
      this._addBlockInBucket(prevPosArr, type);
      const event = new CustomEvent("RotateRecover");
      window.dispatchEvent(event);
    } else {
      // 這裡要修正 pos, 讓任何時候都可以旋轉
      // 計算超過幾個 col, 扣回來
      const posColDiff = isTouchWall
        ? BucketUtility.getColOverAmount(currPosArr, BucketWidth)
        : 0;
      const modifiedCurrPosArr = BucketUtility.updateCurrPosByColDiff(
        currPosArr,
        posColDiff
      );

      this._addBlockInBucket(modifiedCurrPosArr, type);
      this._updateRowCount(modifiedCurrPosArr, prevPosArr);

      if (isTouchWall && posColDiff !== 0) {
        const event = new CustomEvent("ShiftPosCol", {
          detail: {
            posColDiff
          }
        });
        window.dispatchEvent(event);
      }

      print(this.bucket);
    }

    this._drawOnDOM();
  }

  _deleteBlockInBucket(blockInBucketSpace) {
    blockInBucketSpace.forEach(pos => {
      this.bucket[pos.row][pos.col] = 0;
    });
  }

  _addBlockInBucket(blockInBucketSpace, type) {
    blockInBucketSpace.forEach(pos => (this.bucket[pos.row][pos.col] = type));
  }

  _updateRowCount(currPosArr, prevPosArr) {
    prevPosArr.forEach(pos => this.rowCount[pos.row]--);
    currPosArr.forEach(pos => this.rowCount[pos.row]++);
  }

  _updateRowCountByBucket() {
    // 很暴力, 待優化

    this.bucket.forEach((rowArr, idx) => {
      this.rowCount[idx] = rowArr.reduce((prev, curr) => {
        return curr !== 0 ? prev + 1 : prev;
      }, 0);
    });
  }

  /**
   *
   *  @param {Array[Array]} blockShape
   *  @param {Position} refPos - blockShape 在 bucket 的位置
   *  @param {Position} anchorPos - blockShape 中作為轉軸點的位置
   *
   *  @return {Array[Position]} mapArray - blockShape 在 bucket 的座標
   */
  _convertToBucketSpace(blockShape, refPos, anchorPos) {
    /**
     *  blockShape = [[1,1,1]
     *                [0,1,0]]
     *
     *  (posRow, posCol) = (1,2)
     *
     *  map = (0,0) -> (1,2)
     *        (0,1) -> (1,3)
     *        (0,2) -> (1,4)
     *        (1,1) -> (2,3)
     */
    if (refPos.row === -1 && refPos.col === -1) {
      return [];
    }

    let mapArray = [];
    blockShape.forEach((rowArr, row) => {
      rowArr.forEach((ele, col) => {
        if (ele !== 0) {
          mapArray.push(
            new Position({
              row: row + refPos.row - anchorPos.row,
              col: col + refPos.col - anchorPos.col
            })
          );
        }
      });
    });

    return mapArray;
  }

  // 這可以優化成, 與 updateBucket 一樣, 只要更新 currPosArr, prevPosArr 裡面的座標就好
  _drawOnDOM() {
    const event = new CustomEvent("Draw", {
      detail: {
        bucket: this.bucket.slice()
      }
    });
    window.dispatchEvent(event);
  }
}

function print(arr) {
  let str = "";
  arr.forEach(ele => (str += ele.join(" ") + "\n"));
  console.log(str);
}

module.exports = {
  Bucket,
  BucketWidth,
  BucketHeight
};
